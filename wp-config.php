<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hpwp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V<tmfE;-|[31v/,/Bl o-iyA:*iH f[M^j]f4y|>-b~:^[-|?a]Ta*I/Z!)(-Vs9');
define('SECURE_AUTH_KEY',  'Atl~Xf%ulL7LvY/]L>qB[G{* &9L_158+$UTt^-txXE]?i@D>V.=|5F+NvHHGOVo');
define('LOGGED_IN_KEY',    'Jm~^YJsAA.+$7S(nL6wnlE+ZgS2#||l.R,<i>hpkJ1|isHwY/qNXsNIbH&F4t8my');
define('NONCE_KEY',        'z .qg%(o%&:,]lH0sQ?o>q|hv!LiBat+a)9wW]IJf3I3q)we<O-bp2wFH+P-2d?h');
define('AUTH_SALT',        '#L-`Gg8E%/]-LiGYR,k-DD6M^z?&;~O=%_(;s&|lMTh,ft|!)In|>/_LAiaiz) 7');
define('SECURE_AUTH_SALT', 'HjF@7riqM5*%GP8P{otvw+R,@X(w6ofu9Vz@@Ycb|W};+!@fB+Jc:(Jq!V]CaSf+');
define('LOGGED_IN_SALT',   '}M5[he:W,zqVHn4Ol.hb}zfRL0O|Ml#Q0M#o/TkxTr|?P.1bD3*DB.py`HXsaMMt');
define('NONCE_SALT',       ';)|`h_aFph)BO3$@/+2RD+t/,=F{.>id)3uDRH{}*t=JlmKq}?HMsINQ@T1L(Zjo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Multisite */


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
