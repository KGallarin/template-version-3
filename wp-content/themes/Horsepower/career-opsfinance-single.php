<?php
/**
 * The template for displaying all single posts.
 *
 * @package parallax-one
 */

	get_header(); 
?>

	</div>
	<!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->
<section class="welcome-main">
		<div class="welcom-cont">
			<div class="greet">
			Hello, <span>Captain</span>
		</div>
		<div class="small-txt">
			The team is waiting for you!
			<br />
			<br />
			<br />
			<a href="#content" title="Careers design" data-uk-smooth-scroll="">
			Join our team!
			<!-- <i class="uk-icon uk-icon-arrow-circle-o-down"></i> --></a>
		</div>
		</div>
			<div class="homepage-hero-module">
			    <div class="video-container">
			        <div class="filter"></div>
			        <video autoplay loop class="fillWidth">
			            <source src="https://s3-us-west-2.amazonaws.com/coverr/mp4/White-Board.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
			            <source src="https://s3-us-west-2.amazonaws.com/coverr/mp4/White-Board.mp4" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
			        </video>
			        <div class="poster hidden">
			            <img src="PATH_TO_JPEG" alt="">
			        </div>
			    </div>
			</div>
</section>
<div class="content-wrap" id="content">
	<div class="container">
	<div class="uk-width-7-10 uk-container-center">
				<div id="primary" class="content-area <?php if ( is_active_sidebar( 'sidebar' ) ) { echo 'uk-width-1-1 ';} else {echo 'uk-width-1-1';}  ?>">
			<main itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage" id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>
				
				<?php the_post_navigation();  ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					// if ( comments_open() || get_comments_number() ) :
					// 	comments_template();
					// endif;
				?>

			<?php endwhile; // end of the loop. ?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div>


		<?php //get_sidebar(); ?>

	</div>
</div><!-- .content-wrap -->

		<section class="uk-width-1-1 careers-cont uk-margin-large-top">
			<section class="hp-main-cont">
					<div class="uk-grid uk-grid-small hp-headers">
						<div class="uk-width-medium-1-1 uk-width-small-1-1 hp-texts">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-width-small-1 hp-c-flexer">
									<h2 class="uk-text-left uk-margin-bottom-remove uk-padding-remove title-c">
										We are <span data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:400, repeat: true}">HORSEPOWER!</span>
									</h2>		
								</div>
								<div class="uk-width-medium-1-2 uk-width-small-1">
									<h4 class="uk-margin-top">
										Inspiring. Empowering. Rewarding. Fun. These are some of the words people commonly use to describe their careers at Horsepower!
									</h4>	
									<div class="uk-width-1-1">
										<br />
											<a href="careers.php" title="" class="uk-button hp-header-btns hp-rstory uk-margin-remove">Join our team!</a>
										<br />
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="uk-grid career-title">
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="#" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Engineering
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://hp.dev/category/deisgnery/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Design
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="#" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Sales and Marketing (Community, Field Sales, Affiliate, Digital Marketing)
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="#" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Product and Business Development
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="#" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Operations and Finance
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="#" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Social (Client services, PR and Media relations)
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
					</div>
<?php get_footer(); ?>

