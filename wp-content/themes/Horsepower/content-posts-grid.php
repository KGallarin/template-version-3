<?php
/**
 * @package parallax-one
 */
?>
	<div class="uk-width-medium-1-3">
		<article itemscope itemprop="blogPosts" itemtype="http://schema.org/BlogPosting" itemtype="http://schema.org/BlogPosting" <?php post_class('border-bottom-hover blog-post-wrap'); ?> title="<?php printf( esc_html__( 'Blog post: %s', 'parallax-one' ), get_the_title() )?>">
	<header class="entry-header">
			
<!-- 			<div class="entry-meta list-post-entry-meta"> -->	
			<!-- <span class="posted-in entry-terms-categories" itemprop="articleSection"> -->
					<!-- <i class="icon-basic-elaboration-folder-check"></i>Posted in  -->
					<?php
						/* translators: used between list items, there is a space after the comma */
						// $categories_list = get_the_category_list( esc_html__( ', ', 'parallax-one' ) );
						// $pos = strpos($categories_list, ',');
						// if ( $pos ) {
						// 	echo substr($categories_list, 0, $pos);
						// } else {
						// 	echo $categories_list;
						// }
					?>
				<!-- </span> -->
			<!-- </div --><!-- .entry-meta -->
		<div>
			<?php the_title( sprintf( '<h2 class="entry-title" itemprop="headline" style="font-size: 20px;"><a href="%s" rel="bookmark" style="text-decoration:none;">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		</div>
		<span class="uk-float-left" style="font-size:12px;"><?php the_time('l, F jS, Y') ?></span>
		<span itemscope itemprop="author" itemtype="http://schema.org/Person" class="entry-author post-author">
			<div class="uk-clearfix">
		</span>
		<div class="colored-line-left"></div>
		<div class="clearfix"></div>
		<hr class="uk-divider" style="margin-top: 5px;">
	</header><!-- .entry-header -->
	<div itemprop="description" class="entry-content entry-summary">
		<?php
			$ismore = @strpos( $post->post_content, '<!--more-->');
			if($ismore) : the_content( sprintf( esc_html__('Read more %s ...','parallax-one'), '<span class="screen-reader-text">'.esc_html__('about ', 'parallax-one').get_the_title().'</span>' ) );
			else : echo get_excerpt();
			endif;
		?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'parallax-one' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
	</div>