<?php 
/*
Template Name: Plans and Pricing
*/

    get_header(); 
?>

    </div>
    <!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->
<style>
    .uk-tab-center .uk-tab>li>a {
        text-align: center;
        padding: 4px 31px;
        background: rgba(255,255,255,0.15);
        border-radius: 0;
        margin: 0;
        text-shadow: none;
        color: #333;
        border-radius: 0px;
        border: 1px solid transparent;
    }

    .uk-tab-center .uk-tab>li>a:hover, .uk-tab-center .uk-tab>li>a:focus, .uk-tab-center .uk-tab>li>a:active{
        color: #000 !important;
        background: #fff;
        border: 1px solid #C12A2F;
        border-radius: 2px;
    }
    .uk-tab-center {
        border-bottom: none;
    }
    .uk-tab>li.uk-active>a {
        border-bottom-color: transparent;
        background: #fff !important;
        color: #C21D22;
        box-shadow: 0px 1px 7px rgba(51, 51, 51, 0.3);
        font-weight: bolder;
       border-top: 1px solid #C21D22;
        border-bottom: 1px solid #C21D22;
    }

    .uk-tab-center .uk-tab>li {
        position: relative;
        right: -50%;
        margin: 0 40px !important;
    }
    .uk-tab-center .hp-tab>li {
        position: relative;
        right: -50%;
        margin: 0 23px !important;
    }
    
</style>
<section class="uk-width-medium-1-1 uk-width-small-1-1 hp-imgcont">
        <img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pricing.jpg" title="Horsepower" alt="Horsepower" style="width: 100%;
        height: 135px;">
</section>
<div class="uk-container-center"></div>
<section class="width-full">
    <div class="uk-width-medium-1-1 uk-row-first uk-tab-center" style="margin-top: -5.5%;">
                    <ul class="uk-tab hp-tab" data-uk-tab="{connect:'#tab-content-a-slide-horizontal', animation: ''}" >
                        <li class="uk-active" aria-expanded="true">
                            <div class="uk-button-dropdown uk-text-center" data-uk-dropdown="{mode:'hover', pos:'bottom-center'}" aria-haspopup="true" aria-expanded="true" style="cursor:pointer;">
                                <a href="javascript:void(0);" class="drp-trigger"></a>
                                <b class="m-price"><span class="psos">PHP</span>0<span class="unit">/Day</span><br/ ></b><b class="b-type">
                                    Business Lite
                                </b>
                                <div class="uk-dropdown uk-dropdown-bottom navtab-derp">
                                <h4>Business Lite</h4>
                                    <p>You want the convenience of paying bills online.  And needs free advice on legal, tax and acconting matters.</p>
                                    <p>Want to avail of SEC/DTI registration, permits, BIR filings, bookeeping, and others.</p>
                            </div>
                        </li>
                        <li class="">
                            <div class="uk-button-dropdown uk-text-center" data-uk-dropdown="{mode:'hover', pos:'bottom-center'}" aria-haspopup="true" aria-expanded="true" style="cursor:pointer;">
                                <a href="javascript:void(0);" class="drp-trigger"></a>
                                 <b class="m-price"><span class="psos">PHP</span>2.80<span class="unit">/Day</span><br/ ></b>
                                 <b class="b-type">
                                    Business Basic
                                </b>
                                <div class="uk-dropdown uk-dropdown-bottom navtab-derp">
                                <h4>Business Basic</h4>
                                    <p>Same benefits as Lite, plus 20% discount on doctor visits and hospitalization.</p>
                                    <p>Health Benefits are transferrable to your dependents.</p>
                            </div>
                        </li>
                        <li class="">
                            <div class="uk-button-dropdown uk-text-center" data-uk-dropdown="{mode:'hover', pos:'bottom-center'}" aria-haspopup="true" aria-expanded="true" style="cursor:pointer;">
                                <a href="javascript:void(0);" class="drp-trigger"></a>
                                <b class="m-price"><span class="psos">PHP</span>4.70<span class="unit">/Day</span><br/ ></b><b class="b-type">
                                    Business Bronze
                                </b>
                                <div class="uk-dropdown uk-dropdown-bottom navtab-derp">
                                    <h4> Business Bronze</h4>
                                    <p>You believe you are very healthy but still wants manage a one-time risk.</p>
                                    <p>Same benefits as Basic, but has a P30,000 one-time hospitalization coverage</p>
                            </div>
                        </li>
                        <li class="">
                            <div class="uk-button-dropdown uk-text-center" data-uk-dropdown="{mode:'hover', pos:'bottom-center'}" aria-haspopup="true" aria-expanded="true" style="cursor:pointer;">
                                <a href="javascript:void(0);" class="drp-trigger"></a>
                                 <b class="m-price"><span class="psos">PHP</span>19.20<span class="unit">/Day</span><br/ ></b><b class="b-type">
                                    Business Silver
                                </b>
                                <div class="uk-dropdown uk-dropdown-bottom navtab-derp">
                                <h4>Business Silver</h4>
                                    <p>You want want a healtchare coverage worth P30,000 per illness coverage.</p>
                                    <p>Free accounting and payoll software.  You want paying bills online.  You want free advice on legal, tax and accounting matters. Discounts on co-working spaces.</p>
                                    <p>Want to avail of SEC/DTI registration, permits, BIR filings, bookeeping, and others.</p>
                            </div>
                        </li>
                        <li class="">
                            <div class="uk-button-dropdown uk-text-center" data-uk-dropdown="{mode:'hover', pos:'bottom-center'}" aria-haspopup="true" aria-expanded="true" style="cursor:pointer;">
                                <a href="javascript:void(0);" class="drp-trigger"></a>
                                 <b class="m-price"><span class="psos">PHP</span>30.30<span class="unit">/Day</span><br/ ></b><b class="b-type">
                                    Business Gold
                                </b>
                                <div class="uk-dropdown uk-dropdown-bottom navtab-derp">
                                <h4>Business Gold</h4>
                                    <p>Same benefits as Silver, but with P75,000 healthcare coverage per illness</p>
                            </div>
                        </li>
                        <li class="">
                            <div class="uk-button-dropdown uk-text-center" data-uk-dropdown="{mode:'hover', pos:'bottom-center'}" aria-haspopup="true" aria-expanded="true" style="cursor:pointer;">
                                <a href="javascript:void(0);" class="drp-trigger"></a>
                                 <b class="m-price"><span class="psos">PHP</span>41.40<span class="unit">/Day</span><br/ ></b><b class="b-type">
                                    Business Platinum
                                </b>
                                <div class="uk-dropdown uk-dropdown-bottom navtab-derp">
                                <h4>Business Platinum</h4>
                                    <p>Same benefits as Silver, but with P100,000 healthcare coverage per illness</p>
                            </div>
                        </li>

                    <li class="uk-tab-responsive uk-active uk-hidden" aria-haspopup="true" aria-expanded="false"><a>Tab</a><div class="uk-dropdown uk-dropdown-small">
                    <ul class="uk-nav uk-nav-dropdown"></ul>
                    </li>
                    </ul>
                    <br/>
                    <br />
                    <ul id="tab-content-a-slide-horizontal" class="uk-switcher cust-switchers">
                        <li class="uk-active" style="animation-duration: 200ms;">
                            <div class="uk-panel uk-panel-box cust-panel crrent" data-uk-sticky="{ top:73,boundary:'top:-500'}">Business Lite</div>
                                <br />
                            <div class="container list-cont">
                            <br />
                                <div class="uk-grid">
                                    <div class="uk-width-small-1-2">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                            <br />
                                            <div class="uk-width-1-1 price-nest">
                                               <img class="peso" src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pso.png" title="Horsepower" alt="Horsepower">
                                               <span class="price-no">
                                                        0
                                                    </span>
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p>PER DAY BILLED ANNUALLY</p>
                                                    </div>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                      Entry level membership with full privilege in online bills payment and government postings, general tax and legal support and referral incentives.
                                                    </p>
                                                    <p>
                                                        <h3>Covers :</h3>
                                                    </p>

                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Utility Bills
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Bills Payments</strong></p>
                                                               Pay your utility bills, from water to cable and internet plans, at your convenience! 
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Government Posting
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Government Posting</strong></p>
                                                               Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Tax & Legal Inquiries
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Tax & Legal Inquiries</strong></p>
                                                               Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                                                            </div>
                                                        </div>
                                                    </p>
                                                        <p>
                                                            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                                Referral Program
                                                                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                    <p><strong>Referral Program</strong></p>
                                                                    Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                                                            </div>
                                                        </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- 
                                <hr class="uk-divider">-->
                                <!-- Compliance starts -->
                                    <div class="uk-width-small-1-2">
                                        <div class="uk-grid">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        Listing Business Compliance Services
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                                                    </div>
                                                </p>
                                                <br />
                                                <br />
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Registration
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Registration</strong></p>
                                                                <ul>
                                                                    <li>1. Virtual Office</li>
                                                                    <li>2. SEC</li>
                                                                    <li>3. DTI</li>
                                                                    <li>4. BUSINESS PERMITS</li>
                                                                    <li>5.  Barangay Clearance</li>
                                                                    <li>
                                                                        6.  BIR: Non-Vat/VAT Registration
                                                                        <ul>
                                                                            <li>a.  OR Printing</li>
                                                                            <li>b.  Invoice OR Variations</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>7.  SSS: Employer/Employees</li>
                                                                    <li>8.  Pag-ibig: Employer/Employees</li>
                                                                    <li>9.  Philhealth: Employer/Employees</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Monthly/ Quarterly Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Annual Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Annual</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;Audited Financial Statement</li>
                                                                    <li>2.&nbsp;Income Tax Return</li>
                                                                    <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <br />
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                                <!-- compliance ends -->
                                <div class="uk-width-1-1 uk-margin-large-top">
                                <br />
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/members/register/solo/lite/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                                
                                <div class="uk-width-1-1 uk-margin-large-top">
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/business-lite/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </li>
                        <li class="" style="animation-duration: 200ms;">
                        <div class="uk-panel uk-panel-box cust-panel crrent" data-uk-sticky="{ top:73,boundary:'top:-500'}">Business Basic</div>
                        <br/>
                        <br />
                            <div class="container uk-margin-large-top">
                                <div class="uk-grid">
                                    <div class="uk-width-small-1-2">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                            <br />
                                            <div class="uk-width-1-1 price-nest">
                                                    <img class="peso" src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pso.png" title="Horsepower" alt="Horsepower">
                                                    <span class="price-no">
                                                        2.80
                                                    </span>
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p>PER DAY BILLED ANNUALLY</p>
                                                       <p><b>OR P999 PER YEAR</b></p>
                                                    </div>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                      Entry level membership with full privilege in online bills payment and government postings, 
                                                      general tax and legal support and referral incentives.
                                                    </p>
                                                    <p>
                                                        <h3>Covers :</h3>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Managed Healthcare Services
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Managed Healthcare Services</strong></p>
                                                                Use your prepaid health card in all Healthway clinics and enjoy discounted 
                                                                rates on consultations and laboratory tests without annual limits for you and 
                                                                your family.
                                                                <br />
                                                                <br />
                                                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Life & Accident Insurance
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Life & Accident Insurance</strong></p>
                                                                Use your prepaid health card in all Healthway clinics and enjoy discounted 
                                                                rates on consultations and laboratory tests without annual limits for you and 
                                                                your family.                                                         
                                                            </div>
                                                        </div>
                                                    </p>

                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Bills Payments
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Bills Payments</strong></p>
                                                               Pay your utility bills, from water to cable and internet plans, at your convenience! 
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Government Posting
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Government Posting</strong></p>
                                                               Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Tax & Legal Inquiries
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Tax & Legal Inquiries</strong></p>
                                                               Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                                                            </div>
                                                        </div>
                                                    </p>
                                                        <p>
                                                            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                                Referral Program
                                                                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                    <p><strong>Referral Program</strong></p>
                                                                    Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                                                            </div>
                                                        </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Co-working Space
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Co-working Space</strong></p>
                                                                Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Business Cards
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Business Cards</strong></p>
                                                                Get discounted rates.
                                                            </div>
                                                        </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-small-1-2 uk-container-center ">
                                        <div class="uk-grid">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        Listing Business Compliance Services
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                                                    </div>
                                                </p>
                                                <br />
                                                <br />
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        Managed Health Care Benefit Accredited Clinics
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>List of Accredited Clinics</b></p>
                                                    </div>
                                                </p>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Registration
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Registration</strong></p>
                                                                <ul>
                                                                    <li>1. Virtual Office</li>
                                                                    <li>2. SEC</li>
                                                                    <li>3. DTI</li>
                                                                    <li>4. BUSINESS PERMITS</li>
                                                                    <li>5.  Barangay Clearance</li>
                                                                    <li>
                                                                        6.  BIR: Non-Vat/VAT Registration
                                                                        <ul>
                                                                            <li>a.  OR Printing</li>
                                                                            <li>b.  Invoice OR Variations</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>7.  SSS: Employer/Employees</li>
                                                                    <li>8.  Pag-ibig: Employer/Employees</li>
                                                                    <li>9.  Philhealth: Employer/Employees</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Monthly/ Quarterly Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Annual Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Annual</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;Audited Financial Statement</li>
                                                                    <li>2.&nbsp;Income Tax Return</li>
                                                                    <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <br />
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway Alabang Town Center 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Address</strong></p>
                                                                2/F Alabang Town Center<br />
                                                                Alabang-Zapote Road<br />
                                                                Alabang, Muntinlupa City
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway EDSA Shangri-La Plaza Mall 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                5/F Wellness Zone, Shangri-La Plaza Mall<br />
                                                                EDSA Corner Shaw Boulevard<br />
                                                                Mandaluyong City
                                                            </div>
                                                        </div>
                                                    </p>
                                                   <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway Festival Mall 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                2/F Pixie Forest Entrance<br />
                                                                Filinvest Corporate City<br />
                                                                Alabang, Muntinlupa City
                                                            </div>
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway Greenbelt 5 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                4/F Greenbelt 5 Ayala Center<br />
                                                                Makati City<br />
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway Manila 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                Padre Faura Street corner J. Bocobo Street<br />
                                                                Malate, Manila<br />
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway SM North The Block 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                5/F The Block<br />
                                                                SM City North EDSA<br />
                                                                Quezon City
                                                            </div>
                                                        </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <hr class="uk-divider">-->
                                <div class="uk-width-1-1 uk-margin-large-top">
                                <br />
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/members/register/solo/basic/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                                <div class="uk-width-1-1 uk-margin-large-top">
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/business-basic/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </li>
                        <li class="" style="animation-duration: 200ms;">
                        <div class="uk-panel uk-panel-box cust-panel crrent" data-uk-sticky="{ top:73,boundary:'top:-500'}">Business Bronze</div>
                        <br/>
                        <br />
                                     <div class="container uk-margin-large-top">
                                <div class="uk-grid">
                                    <div class="uk-width-small-1-2">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                            <br />
                                            <div class="uk-width-1-1 price-nest">
                                                    <img class="peso" src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pso.png" title="Horsepower" alt="Horsepower">
                                                    <span class="price-no">
                                                        4.70
                                                    </span>
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p>PER DAY BILLED ANNUALLY</p>
                                                       <p><b>OR P1,699 PER YEAR</b></p>
                                                    </div>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                      Entry level membership with full privilege in online bills payment and government postings, managed healthcare services, minimum life and accident insurance and free access to international medical assistance. 
                                                      Enjoy 25 hours a month co-working space benefit from our selected partners, general tax and legal support, retail rewards and referral incentives.
                                                    </p>
                                                    <p>
                                                        <h3>Covers :</h3>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Managed Healthcare Services
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Managed Healthcare Services</strong></p>
                                                                Use your prepaid health card in all Healthway clinics and enjoy discounted 
                                                                rates on consultations and laboratory tests without annual limits for you and 
                                                                your family.
                                                                <br />
                                                                <br />
                                                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                          
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Life & Accident Insurance
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Life & Accident Insurance</strong></p>
                                                                Get <b>double coverage</b> on life and accident insurance plus <b>FREE</b> access to international medical assistance and emergency services.                                                        
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a> 
                                                            Emergency Hospitalization
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Emergency Hospitalization</strong></p>
                                                                One-Time Hospitalization Emergency worth Php 15,000.
                                                            </div> 
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Bills Payments
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Bills Payments</strong></p>
                                                               Pay your utility bills, from water to cable and internet plans, at your convenience! 
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Government Posting
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Government Posting</strong></p>
                                                               Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Tax & Legal Inquiries
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Tax & Legal Inquiries</strong></p>
                                                               Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                                                            </div>
                                                        </div>
                                                    </p>
                                                        <p>
                                                            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                                Referral Program
                                                                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                    <p><strong>Referral Program</strong></p>
                                                                    Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                                                            </div>
                                                        </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Co-working Space
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Co-working Space</strong></p>
                                                                Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Business Cards
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Business Cards</strong></p>
                                                                Get discounted rates.
                                                            </div>
                                                        </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Compliance starts -->
                                                                <div class="uk-width-small-1-2 uk-container-center ">
                                        <div class="uk-grid">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        Listing Business Compliance Services
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                                                    </div>
                                                </p>
                                                <br />
                                                <br />
                                                <br />
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        Managed Health Care Benefit Clinics
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>List of Accredited Clinics</b></p>
                                                    </div>
                                                </p>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Registration
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Registration</strong></p>
                                                                <ul>
                                                                    <li>1. Virtual Office</li>
                                                                    <li>2. SEC</li>
                                                                    <li>3. DTI</li>
                                                                    <li>4. BUSINESS PERMITS</li>
                                                                    <li>5.  Barangay Clearance</li>
                                                                    <li>
                                                                        6.  BIR: Non-Vat/VAT Registration
                                                                        <ul>
                                                                            <li>a.  OR Printing</li>
                                                                            <li>b.  Invoice OR Variations</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>7.  SSS: Employer/Employees</li>
                                                                    <li>8.  Pag-ibig: Employer/Employees</li>
                                                                    <li>9.  Philhealth: Employer/Employees</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Monthly/ Quarterly Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Annual Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Annual</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;Audited Financial Statement</li>
                                                                    <li>2.&nbsp;Income Tax Return</li>
                                                                    <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <br />
                                                    <br />
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway Alabang Town Center 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Address</strong></p>
                                                                2/F Alabang Town Center<br />
                                                                Alabang-Zapote Road<br />
                                                                Alabang, Muntinlupa City
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway EDSA Shangri-La Plaza Mall 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                5/F Wellness Zone, Shangri-La Plaza Mall<br />
                                                                EDSA Corner Shaw Boulevard<br />
                                                                Mandaluyong City
                                                            </div>
                                                        </div>
                                                    </p>
                                                   <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway Festival Mall 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                2/F Pixie Forest Entrance<br />
                                                                Filinvest Corporate City<br />
                                                                Alabang, Muntinlupa City
                                                            </div>
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway Greenbelt 5 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                4/F Greenbelt 5 Ayala Center<br />
                                                                Makati City<br />
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway Manila 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                Padre Faura Street corner J. Bocobo Street<br />
                                                                Malate, Manila<br />
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Healthway SM North The Block 
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                <p><strong>Address</strong></p>
                                                                5/F The Block<br />
                                                                SM City North EDSA<br />
                                                                Quezon City
                                                            </div>
                                                        </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- compliance ends -->
                                </div><!-- 
                                <hr class="uk-divider">-->

                                    
                                <div class="uk-width-1-1 uk-margin-large-top">
                                <br />
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/members/register/solo/bronze/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                                <div class="uk-width-1-1 uk-margin-large-top">
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/business-bronze/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </li>
                        <li class="" style="animation-duration: 200ms;">
                                <div class="uk-panel uk-panel-box cust-panel crrent" data-uk-sticky="{ top:73,boundary:'top:-500'}">Business Silver</div>
                        <br/>
                        <br />
                             <div class="container uk-margin-large-top">
                                <div class="uk-grid">
                                    <div class="uk-width-small-1-2">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                            <br />
                                            <div class="uk-width-1-1 price-nest">
                                                    <img class="peso2" src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pso.png" title="Horsepower" alt="Horsepower">
                                                    <span class="price-no">
                                                        19.20
                                                    </span>
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p>PER DAY BILLED ANNUALLY</p>
                                                       <p><b>OR P6,899 PER YEAR</b></p>
                                                    </div>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        Standard membership package with full privilege in online bills payment and government postings, healthcare insurance plan, double coverage life and accident insurance and international medical assistance. Enjoy 25 hours a month co-working space benefit from our selected partners, 
                                                        general tax and legal advice, basic accounting software, retail rewards and referral incentives.
                                                    </p>
                                                    <p>
                                                        <h3>Covers :</h3>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Free Accounting and Payroll Software
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Free Accounting and Payroll Software</strong></p>
                                                                Accounting and Payroll ready software is available for use.                                                 
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Managed Healthcare Services
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Managed Healthcare Services</strong></p>
                                                                Use your prepaid health card in all Healthway clinics and enjoy discounted 
                                                                rates on consultations and laboratory tests without annual limits for you and 
                                                                your family.
                                                                <br />
                                                                <br />
                                                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Health Care Insurance up to PHP 30,000 MBL <br />(Maximum Benefit Limit)
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Health Care Insurance</strong></p>
                                                                Members can avail up to PHP 30,000 MBL per sickness with cashless transactions 
                                                                on accredited hospitals and clinics. Pre-existing condition not covered. 
                                                                <br />
                                                                <br /> 
                                                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                      
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Life & Accident Insurance
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Life & Accident Insurance</strong></p>
                                                                Get <b>double coverage</b> on life and accident insurance plus <b>FREE</b> access to international medical assistance and emergency services.                                                        
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <!-- <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a> -->
                                                            Travel Insurance (Local & International)
                                                            <!-- <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div> -->
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Bills Payments
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Bills Payments</strong></p>
                                                               Pay your utility bills, from water to cable and internet plans, at your convenience! 
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Government Posting
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Government Posting</strong></p>
                                                               Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Tax & Legal Inquiries
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Tax & Legal Inquiries</strong></p>
                                                               Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                                                            </div>
                                                        </div>
                                                    </p>
                                                        <p>
                                                            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                                Referral Program
                                                                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                    <p><strong>Referral Program</strong></p>
                                                                    Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                                                            </div>
                                                        </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Co-working Space
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Co-working Space</strong></p>
                                                                Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Business Cards
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Business Cards</strong></p>
                                                                Get discounted rates.
                                                            </div>
                                                        </div>
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Compliance starts -->
                                    <div class="uk-width-small-1-2 uk-container-center ">
                                        <div class="uk-grid">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        Listing Business Compliance Services
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                                                    </div>
                                                </p>
                                                <br />
                                                <br />
                                                <br />
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        TOP Hospitals Included
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>HOSPITALS THAT ARE INCLUDED ON PACKAGES</b></p>
                                                    </div>
                                                </p>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Registration
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Registration</strong></p>
                                                                <ul>
                                                                    <li>1. Virtual Office</li>
                                                                    <li>2. SEC</li>
                                                                    <li>3. DTI</li>
                                                                    <li>4. BUSINESS PERMITS</li>
                                                                    <li>5.  Barangay Clearance</li>
                                                                    <li>
                                                                        6.  BIR: Non-Vat/VAT Registration
                                                                        <ul>
                                                                            <li>a.  OR Printing</li>
                                                                            <li>b.  Invoice OR Variations</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>7.  SSS: Employer/Employees</li>
                                                                    <li>8.  Pag-ibig: Employer/Employees</li>
                                                                    <li>9.  Philhealth: Employer/Employees</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Monthly/ Quarterly Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Annual Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Annual</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;Audited Financial Statement</li>
                                                                    <li>2.&nbsp;Income Tax Return</li>
                                                                    <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <br />
                                                    <br />
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Asian Hospital and Medical Center 
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            Makati Medical Center
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            St Lukes QC
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            The Medical City
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Cardinal Santos Medical Center
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            World Citi Medical Center
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Manila Doctors Hospital
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            UST Hospital
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Reimbursment</strong></p>
                                                                This Hospital is not accredited but our partners will reimburse the expense.
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            St. Lukes BGC
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Reimbursment</strong></p>
                                                                This Hospital is not accredited but our partners will reimburse the expense.
                                                            </div>
                                                        </div>
                                                    </p> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                               
                                <!-- compliance ends -->
                                </div><!-- 
                                <hr class="uk-divider">-->
                                                                
                                <div class="uk-width-1-1 uk-margin-large-top">
                                <br />
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/members/register/solo/silver/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                                <div class="uk-width-1-1 uk-margin-large-top">
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/business-silver/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </li>
                        <li class="" style="animation-duration: 200ms;">
                         <div class="uk-panel uk-panel-box cust-panel crrent" data-uk-sticky="{ top:73,boundary:'top:-500'}">Business Gold</div>
                        <br/>
                        <br />
                             <div class="container uk-margin-large-top">
                                <div class="uk-grid">
                                    <div class="uk-width-small-1-2">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                            <br />
                                            <div class="uk-width-1-1 price-nest">
                                                    <img class="peso2" src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pso.png" title="Horsepower" alt="Horsepower">
                                                    <span class="price-no">
                                                        30.30
                                                    </span>
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p>PER DAY BILLED ANNUALLY</p>
                                                       <p><b>OR P10,895 PER YEAR</b></p>
                                                    </div>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        Mid level membership with full privilege in online bills payment and government postings, 
                                                        healthcare insurance plan, double coverage life and accident insurance and international medical assistance. Enjoy 25 hours a month co-working space benefit from our selected partners, 
                                                        general tax and legal advice, basic accounting software, retail rewards and referral incentives.                                                  </p>
                                                    <p>
                                                        <h3>Covers :</h3>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Free Accounting and Payroll Software
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Free Accounting and Payroll Software</strong></p>
                                                                Accounting and Payroll ready software is available for use.                                                  
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Managed Healthcare Services
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Managed Healthcare Services</strong></p>
                                                                Use your prepaid health card in all Healthway clinics and enjoy discounted 
                                                                rates on consultations and laboratory tests without annual limits for you and 
                                                                your family.
                                                                <br />
                                                                <br />
                                                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Health Care Insurance up to PHP 75,000 MBL <br />(Maximum Benefit Limit)
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Health Care Insurance</strong></p>
                                                                Members can avail <b>up to PHP 75,000 MBL</b> per sickness with 
                                                                cashless transactions on accredited hospitals and clinics. Pre-existing condition not covered.                                                     
                                                                <br />
                                                                <br />
                                                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b> 
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Life & Accident Insurance
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Life & Accident Insurance</strong></p>
                                                                Get <b>double coverage</b> on life and accident insurance plus <b>FREE</b> access to international medical assistance and emergency services.                                                        
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <!-- <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a> -->
                                                            Travel Insurance (Local & International)
                                                            <!-- <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div> -->
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Bills Payments
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Bills Payments</strong></p>
                                                               Pay your utility bills, from water to cable and internet plans, at your convenience! 
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Government Posting
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Government Posting</strong></p>
                                                               Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Tax & Legal Inquiries
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Tax & Legal Inquiries</strong></p>
                                                               Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                                                            </div>
                                                        </div>
                                                    </p>
                                                        <p>
                                                            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                                Referral Program
                                                                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                    <p><strong>Referral Program</strong></p>
                                                                    Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                                                            </div>
                                                        </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Co-working Space
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Co-working Space</strong></p>
                                                                Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Business Cards
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Business Cards</strong></p>
                                                                Get discounted rates.
                                                            </div>
                                                        </div>
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Compliance starts -->
                                                                <div class="uk-width-small-1-2 uk-container-center ">
                                        <div class="uk-grid">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        Listing Business Compliance Services
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                                                    </div>
                                                </p>
                                                <br />
                                                <br />
                                                <br />
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        TOP Hospitals Included
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>HOSPITALS THAT ARE INCLUDED ON PACKAGES</b></p>
                                                    </div>
                                                </p>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Registration
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Registration</strong></p>
                                                                <ul>
                                                                    <li>1. Virtual Office</li>
                                                                    <li>2. SEC</li>
                                                                    <li>3. DTI</li>
                                                                    <li>4. BUSINESS PERMITS</li>
                                                                    <li>5.  Barangay Clearance</li>
                                                                    <li>
                                                                        6.  BIR: Non-Vat/VAT Registration
                                                                        <ul>
                                                                            <li>a.  OR Printing</li>
                                                                            <li>b.  Invoice OR Variations</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>7.  SSS: Employer/Employees</li>
                                                                    <li>8.  Pag-ibig: Employer/Employees</li>
                                                                    <li>9.  Philhealth: Employer/Employees</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Monthly/ Quarterly Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Annual Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Annual</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;Audited Financial Statement</li>
                                                                    <li>2.&nbsp;Income Tax Return</li>
                                                                    <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <br />
                                                    <br />
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Asian Hospital and Medical Center 
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            Makati Medical Center
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            St Lukes QC
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            The Medical City
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Cardinal Santos Medical Center
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            World Citi Medical Center
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Manila Doctors Hospital
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            UST Hospital
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Reimbursment</strong></p>
                                                                This Hospital is not accredited but our partners will reimburse the expense.
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            St. Lukes BGC
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Reimbursment</strong></p>
                                                                This Hospital is not accredited but our partners will reimburse the expense.
                                                            </div>
                                                        </div>
                                                    </p> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                <!-- compliance ends -->
                                </div><!-- 
                                <hr class="uk-divider">-->

                                    
                                <div class="uk-width-1-1 uk-margin-large-top">
                                <br />
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/members/register/solo/gold/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                                <div class="uk-width-1-1 uk-margin-large-top">
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/business-gold/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </li>
                        <li class="" style="animation-duration: 200ms;">
                        <div class="uk-panel uk-panel-box cust-panel crrent" data-uk-sticky="{ top:73,boundary:'top:400'}">Business Platinum</div>
                        <br/>
                        <br />
                            <div class="container uk-margin-large-top">
                                <div class="uk-grid">
                                    <div class="uk-width-small-1-2 uk-container-center">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                            <br />
                                            <div class="uk-width-1-1 price-nest">
                                                    <img class="peso2" src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pso.png" title="Horsepower" alt="Horsepower">
                                                    <span class="price-no">
                                                        41.40
                                                    </span>
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p>PER DAY BILLED ANNUALLY</p>
                                                       <p><b>OR P14,899 PER YEAR</b></p>
                                                    </div>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        Premium membership package with full privilege in online bills payment and government postings, healthcare insurance plan, double coverage life and accident insurance and international medical assistance. Enjoy 25 hours a month co-working space benefit from our selected partners, 
                                                        general tax and legal advice, basic accounting software, retails rewards and referral incentives.                                                   <p>
                                                        <h3>Covers :</h3>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Free Accounting and Payroll Software
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Free Accounting and Payroll Software</strong></p>
                                                                Accounting and Payroll ready software is available for use.                                                    
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Managed Healthcare Services
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Managed Healthcare Services</strong></p>
                                                                Use your prepaid health card in all Healthway clinics and enjoy discounted 
                                                                rates on consultations and laboratory tests without annual limits for you and 
                                                                your family.
                                                                <br />
                                                                <br />
                                                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Health Care Insurance up to PHP 100,000 MBL <br />(Maximum Benefit Limit)
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Health Care Insurance</strong></p>
                                                                Members can avail up to PHP 100,000 MBL per sickness with cashless transactions on accredited hospitals and clinics. 
                                                                <b>Pre-existing condition not covered</b>.
                                                                <br />
                                                                <br />
                                                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                            
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Life & Accident Insurance
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Life & Accident Insurance</strong></p>
                                                                Get <b>double coverage</b> on life and accident insurance plus <b>FREE</b> access to international medical assistance and emergency services.                                                        
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <!-- <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a> -->
                                                            Travel Insurance (Local & International)
                                                            <!-- <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div> -->
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Bills Payments
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Bills Payments</strong></p>
                                                               Pay your utility bills, from water to cable and internet plans, at your convenience! 
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Government Posting
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Government Posting</strong></p>
                                                               Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Tax & Legal Inquiries
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Tax & Legal Inquiries</strong></p>
                                                               Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                                                            </div>
                                                        </div>
                                                    </p>
                                                        <p>
                                                            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                                Referral Program
                                                                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                                    <p><strong>Referral Program</strong></p>
                                                                    Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                                                            </div>
                                                        </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Co-working Space
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Co-working Space</strong></p>
                                                                Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Business Cards
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                            <p><strong>Business Cards</strong></p>
                                                                Get discounted rates.
                                                            </div>
                                                        </div>
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <!-- Compliance starts -->
                                    <div class="uk-width-small-1-2 uk-container-center ">
                                        <div class="uk-grid">
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        Listing Business Compliance Services
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                                                    </div>
                                                </p>
                                                <br />
                                                <br />
                                                <br />
                                                <p>
                                                    <div class="uk-width-1-1 price-nest sep-cat">
                                                        TOP Hospitals Included
                                                   </div>
                                                   <div class="uk-width-1-1 price-info">
                                                        <p><b>HOSPITALS THAT ARE INCLUDED ON PACKAGES</b></p>
                                                    </div>
                                                </p>
                                            </div>
                                            <div class="uk-width-small-1-2 uk-container-center">
                                                <div class="uk-margin-small-left">
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Registration
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Registration</strong></p>
                                                                <ul>
                                                                    <li>1. Virtual Office</li>
                                                                    <li>2. SEC</li>
                                                                    <li>3. DTI</li>
                                                                    <li>4. BUSINESS PERMITS</li>
                                                                    <li>5.  Barangay Clearance</li>
                                                                    <li>
                                                                        6.  BIR: Non-Vat/VAT Registration
                                                                        <ul>
                                                                            <li>a.  OR Printing</li>
                                                                            <li>b.  Invoice OR Variations</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>7.  SSS: Employer/Employees</li>
                                                                    <li>8.  Pag-ibig: Employer/Employees</li>
                                                                    <li>9.  Philhealth: Employer/Employees</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Monthly/ Quarterly Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;SSS</li>
                                                                    <li>2.&nbsp;Philhealth</li>
                                                                    <li>3.&nbsp;Pag-ibig</li>
                                                                    <li>4.&nbsp;BIR</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            Annual Filings
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Filings : Annual</strong></p>
                                                                <ul>
                                                                    <li>1.&nbsp;Audited Financial Statement</li>
                                                                    <li>2.&nbsp;Income Tax Return</li>
                                                                    <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <br />
                                                    <br />
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Asian Hospital and Medical Center 
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            Makati Medical Center
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            St Lukes QC
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            The Medical City
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Cardinal Santos Medical Center
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            World Citi Medical Center
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            
                                                            Manila Doctors Hospital
                                                        </div>
                                                    </p>
                                                     <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            UST Hospital
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Reimbursment</strong></p>
                                                                This Hospital is not accredited but our partners will reimburse the expense.
                                                            </div>
                                                        </div>
                                                    </p>
                                                    <p>
                                                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                                            St. Lukes BGC
                                                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                                               <p><strong>Reimbursment</strong></p>
                                                                This Hospital is not accredited but our partners will reimburse the expense.
                                                            </div>
                                                        </div>
                                                    </p> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                <!-- compliance ends -->
                                </div><!-- 
                                <hr class="uk-divider">-->
                               
                                <div class="uk-width-1-1 uk-margin-large-top">
                                <br />
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/members/register/solo/platinum/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                                <div class="uk-width-1-1 uk-margin-large-top">
                                    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                                        <a href="http://horsepower.ph/business-platinum/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                                        
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </li>

                        <!-- for the dropdown responsive -->
                        <li class="" style="animation-duration: 200ms;">Bazinga! <a href="#" data-uk-switcher-item="0">Switch to tab 1</a></li>
                    </ul>
            </div>
</section>
<br />
<br />
<section class="mob-dsplay">
<div class="uk-grid">
    <div class="uk-width-medium-1-2 uk-width-small-1-1">
        <div class="uk-accordion" data-uk-accordion="">
        <h3 class="uk-accordion-title uk-active cust-title">
            <span class="uk-text-center mob-plans">
                <p>Business Lite</p>
                <br />
                <p class="m-price">
                    <span class="psos">PHP</span>0<span class="unit">/Day</span><br/>
                </p>
                <p class="uk-text-center uk-margin-large-top btn-mob"><a href="javascript:void(0);">view more</a></p>
            </span>
        </h3>
        <div class="uk-accordion-content mobcust-content">
            <p>You want the convenience of paying bills online.  And needs free advice on legal, tax and acconting matters.</p>
            <p>Want to avail of SEC/DTI registration, permits, BIR filings, bookeeping, and others.</p>
            <p>
                <h3>Covers :</h3>
                <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'click', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Utility Bills
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Bills Payments</strong></p>
                       Pay your utility bills, from water to cable and internet plans, at your convenience! 
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'click', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Government Posting
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Government Posting</strong></p>
                       Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'click', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Tax & Legal Inquiries
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Tax & Legal Inquiries</strong></p>
                       Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                    </div>
                </div>
            </p>
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'click', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Referral Program
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Referral Program</strong></p>
                            Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                    </div>
                </p>
                <br />
                <p>
                    <div class="uk-width-1-1 price-nest sep-cat">
                        Listing Business Compliance Services
                   </div>
                   <div class="uk-width-1-1 uk-text-left price-info">
                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                    </div>
                </p>
                <br />
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Registration
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Registration</strong></p>
                            <ul>
                                <li>1. Virtual Office</li>
                                <li>2. SEC</li>
                                <li>3. DTI</li>
                                <li>4. BUSINESS PERMITS</li>
                                <li>5.  Barangay Clearance</li>
                                <li>
                                    6.  BIR: Non-Vat/VAT Registration
                                    <ul>
                                        <li>a.  OR Printing</li>
                                        <li>b.  Invoice OR Variations</li>
                                    </ul>
                                </li>
                                <li>7.  SSS: Employer/Employees</li>
                                <li>8.  Pag-ibig: Employer/Employees</li>
                                <li>9.  Philhealth: Employer/Employees</li>
                            </ul>
                        </div>
                    </div>
                </p>
                 <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Monthly/ Quarterly Filings
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Filings : Monthly/ Quarterly</strong></p>
                            <ul>
                                <li>1.&nbsp;SSS</li>
                                <li>2.&nbsp;Philhealth</li>
                                <li>3.&nbsp;Pag-ibig</li>
                                <li>4.&nbsp;BIR</li>
                            </ul>
                        </div>
                    </div>
                </p>
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Annual Filings
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Filings : Annual</strong></p>
                            <ul>
                                <li>1.&nbsp;Audited Financial Statement</li>
                                <li>2.&nbsp;Income Tax Return</li>
                                <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                            </ul>
                        </div>
                    </div>
                </p>
            </p>
            <hr />
            <div class="uk-width-small-1-1 uk-container-center uk-text-center">
                <a href="http://horsepower.ph/members/register/solo/lite/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                
                </a>
            </div>
            <div class="uk-width-1-1 uk-margin-large-top">
                <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                    <a href="http://horsepower.ph/business-lite/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                    
                    </a>
                </div>
            </div> 
            <br />
        </div>
    </div>
    <!-- BASIC HERE -->
    <div class="uk-accordion" data-uk-accordion="">
        <h3 class="uk-accordion-title uk-active cust-title">
            <span class="uk-text-center mob-plans">
                <p>Business Basic</p>
                <br />
                <p class="m-price">
                    <span class="psos">PHP</span>2.80<span class="unit">/Day</span><br/>
                </p>
                <p class="uk-text-center uk-margin-large-top btn-mob"><a href="javascript:void(0);">view more</a></p>
            </span>
        </h3>
        <div class="uk-accordion-content mobcust-content">
            <p>Same benefits as Lite, plus 20% discount on doctor visits and hospitalization.</p>
            <p>Health Benefits are transferrable to your dependents.</p>
            <p>
                <h3>Covers :</h3>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Managed Healthcare Services
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Managed Healthcare Services</strong></p>
                        Use your prepaid health card in all Healthway clinics and enjoy discounted 
                        rates on consultations and laboratory tests without annual limits for you and 
                        your family.
                        <br />
                        <br />
                        <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                           
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Life & Accident Insurance
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Life & Accident Insurance</strong></p>
                        Use your prepaid health card in all Healthway clinics and enjoy discounted 
                        rates on consultations and laboratory tests without annual limits for you and 
                        your family.                                                         
                    </div>
                </div>
            </p>

            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Bills Payments
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Bills Payments</strong></p>
                       Pay your utility bills, from water to cable and internet plans, at your convenience! 
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Government Posting
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Government Posting</strong></p>
                       Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Tax & Legal Inquiries
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Tax & Legal Inquiries</strong></p>
                       Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                    </div>
                </div>
            </p>
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Referral Program
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Referral Program</strong></p>
                            Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                    </div>
                </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Co-working Space
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Co-working Space</strong></p>
                        Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Business Cards
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Business Cards</strong></p>
                        Get discounted rates.
                    </div>
                </div>
            </p>
                <br />
                <p>
                    <div class="uk-width-1-1 price-nest sep-cat">
                        Listing Business Compliance Services
                   </div>
                   <div class="uk-width-1-1 uk-text-left price-info">
                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                    </div>
                </p>
                <br />
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Registration
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Registration</strong></p>
                            <ul>
                                <li>1. Virtual Office</li>
                                <li>2. SEC</li>
                                <li>3. DTI</li>
                                <li>4. BUSINESS PERMITS</li>
                                <li>5.  Barangay Clearance</li>
                                <li>
                                    6.  BIR: Non-Vat/VAT Registration
                                    <ul>
                                        <li>a.  OR Printing</li>
                                        <li>b.  Invoice OR Variations</li>
                                    </ul>
                                </li>
                                <li>7.  SSS: Employer/Employees</li>
                                <li>8.  Pag-ibig: Employer/Employees</li>
                                <li>9.  Philhealth: Employer/Employees</li>
                            </ul>
                        </div>
                    </div>
                </p>
                 <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Monthly/ Quarterly Filings
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Filings : Monthly/ Quarterly</strong></p>
                            <ul>
                                <li>1.&nbsp;SSS</li>
                                <li>2.&nbsp;Philhealth</li>
                                <li>3.&nbsp;Pag-ibig</li>
                                <li>4.&nbsp;BIR</li>
                            </ul>
                        </div>
                    </div>
                </p>
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Annual Filings
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Filings : Annual</strong></p>
                            <ul>
                                <li>1.&nbsp;Audited Financial Statement</li>
                                <li>2.&nbsp;Income Tax Return</li>
                                <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                            </ul>
                        </div>
                    </div>
                </p>
                <br />
                <p>
                    <div class="uk-width-1-1 price-nest sep-cat">
                        Managed Health Care Benefit Accredited Clinics
                   </div>
                   <div class="uk-width-1-1 price-info">
                        <p><b>List of Accredited Clinics</b></p>
                    </div>
                </p>
                <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway Alabang Town Center 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Address</strong></p>
                        2/F Alabang Town Center<br />
                        Alabang-Zapote Road<br />
                        Alabang, Muntinlupa City
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway EDSA Shangri-La Plaza Mall 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        5/F Wellness Zone, Shangri-La Plaza Mall<br />
                        EDSA Corner Shaw Boulevard<br />
                        Mandaluyong City
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway Festival Mall 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        2/F Pixie Forest Entrance<br />
                        Filinvest Corporate City<br />
                        Alabang, Muntinlupa City
                    </div>
                </div>
            </p>
             <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway Greenbelt 5 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        4/F Greenbelt 5 Ayala Center<br />
                        Makati City<br />
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway Manila 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        Padre Faura Street corner J. Bocobo Street<br />
                        Malate, Manila<br />
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway SM North The Block 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        5/F The Block<br />
                        SM City North EDSA<br />
                        Quezon City
                    </div>
                </div>
            </p>
            </p>
            <hr />
            <div class="uk-width-small-1-1 uk-container-center uk-text-center">
                <a href="http://horsepower.ph/members/register/solo/basic/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                
                </a>
            </div>
            <div class="uk-width-1-1 uk-margin-large-top">
                <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                    <a href="http://horsepower.ph/business-basic/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                    
                    </a>
                </div>
            </div> 
            <br />
        </div>
    </div>
    <!-- BRONZE HERE -->
    <div class="uk-accordion" data-uk-accordion="">
        <h3 class="uk-accordion-title uk-active cust-title">
            <span class="uk-text-center mob-plans">
                <p>Business Bronze</p>
                <br />
                <p class="m-price">
                    <span class="psos">PHP</span>4.70<span class="unit">/Day</span><br/>
                </p>
                <p class="uk-text-center uk-margin-large-top btn-mob"><a href="javascript:void(0);">view more</a></p>
            </span>
        </h3>
        <div class="uk-accordion-content mobcust-content">
           <p>You believe you are very healthy but still wants manage a one-time risk.</p>
            <p>Same benefits as Basic, but has a P30,000 one-time hospitalization coverage</p>
            <p>
                <h3>Covers :</h3>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Managed Healthcare Services
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Managed Healthcare Services</strong></p>
                        Use your prepaid health card in all Healthway clinics and enjoy discounted 
                        rates on consultations and laboratory tests without annual limits for you and 
                        your family.
                        <br />
                        <br />
                        <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                          
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Life & Accident Insurance
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Life & Accident Insurance</strong></p>
                        Get <b>double coverage</b> on life and accident insurance plus <b>FREE</b> access to international medical assistance and emergency services.                                                        
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a> 
                    Emergency Hospitalization
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Emergency Hospitalization</strong></p>
                        One-Time Hospitalization Emergency worth Php 15,000.
                    </div> 
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Bills Payments
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Bills Payments</strong></p>
                       Pay your utility bills, from water to cable and internet plans, at your convenience! 
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Government Posting
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Government Posting</strong></p>
                       Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Tax & Legal Inquiries
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Tax & Legal Inquiries</strong></p>
                       Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                    </div>
                </div>
            </p>
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Referral Program
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Referral Program</strong></p>
                            Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                    </div>
                </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Co-working Space
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Co-working Space</strong></p>
                        Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Business Cards
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Business Cards</strong></p>
                        Get discounted rates.
                    </div>
                </div>
            </p>
                <br />
                <p>
                    <div class="uk-width-1-1 price-nest sep-cat">
                        Listing Business Compliance Services
                   </div>
                   <div class="uk-width-1-1 uk-text-left price-info">
                        <p><b>PRODUCTS PRICED SEPARATELY</b></p>
                    </div>
                </p>
                <br />
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Registration
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Registration</strong></p>
                            <ul>
                                <li>1. Virtual Office</li>
                                <li>2. SEC</li>
                                <li>3. DTI</li>
                                <li>4. BUSINESS PERMITS</li>
                                <li>5.  Barangay Clearance</li>
                                <li>
                                    6.  BIR: Non-Vat/VAT Registration
                                    <ul>
                                        <li>a.  OR Printing</li>
                                        <li>b.  Invoice OR Variations</li>
                                    </ul>
                                </li>
                                <li>7.  SSS: Employer/Employees</li>
                                <li>8.  Pag-ibig: Employer/Employees</li>
                                <li>9.  Philhealth: Employer/Employees</li>
                            </ul>
                        </div>
                    </div>
                </p>
                 <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Monthly/ Quarterly Filings
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Filings : Monthly/ Quarterly</strong></p>
                            <ul>
                                <li>1.&nbsp;SSS</li>
                                <li>2.&nbsp;Philhealth</li>
                                <li>3.&nbsp;Pag-ibig</li>
                                <li>4.&nbsp;BIR</li>
                            </ul>
                        </div>
                    </div>
                </p>
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Annual Filings
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                           <p><strong>Filings : Annual</strong></p>
                            <ul>
                                <li>1.&nbsp;Audited Financial Statement</li>
                                <li>2.&nbsp;Income Tax Return</li>
                                <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                            </ul>
                        </div>
                    </div>
                </p>
                <br />
                <p>
                    <div class="uk-width-1-1 price-nest sep-cat">
                        Managed Health Care Benefit Accredited Clinics
                   </div>
                   <div class="uk-width-1-1 price-info">
                        <p><b>List of Accredited Clinics</b></p>
                    </div>
                </p>
                <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway Alabang Town Center 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Address</strong></p>
                        2/F Alabang Town Center<br />
                        Alabang-Zapote Road<br />
                        Alabang, Muntinlupa City
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway EDSA Shangri-La Plaza Mall 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        5/F Wellness Zone, Shangri-La Plaza Mall<br />
                        EDSA Corner Shaw Boulevard<br />
                        Mandaluyong City
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway Festival Mall 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        2/F Pixie Forest Entrance<br />
                        Filinvest Corporate City<br />
                        Alabang, Muntinlupa City
                    </div>
                </div>
            </p>
             <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway Greenbelt 5 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        4/F Greenbelt 5 Ayala Center<br />
                        Makati City<br />
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway Manila 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        Padre Faura Street corner J. Bocobo Street<br />
                        Malate, Manila<br />
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Healthway SM North The Block 
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Address</strong></p>
                        5/F The Block<br />
                        SM City North EDSA<br />
                        Quezon City
                    </div>
                </div>
            </p>
            </p>
            <hr />
            <div class="uk-width-small-1-1 uk-container-center uk-text-center">
                <a href="http://horsepower.ph/members/register/solo/bronze/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
                
                </a>
            </div>
            <div class="uk-width-1-1 uk-margin-large-top">
                <div class="uk-width-small-1-2 uk-container-center uk-text-center">
                    <a href="http://horsepower.ph/business-bronze/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
                    
                    </a>
                </div>
            </div> 
        </div>
    </div>
</div>







<!-- SECOND ROW -->
       <div class="uk-width-medium-1-2 uk-width-small-1-1">
        <div class="uk-accordion" data-uk-accordion="">
        <h3 class="uk-accordion-title uk-active cust-title">
            <span class="uk-text-center mob-plans">
                <p>Business Silver</p>
                <br />
                <p class="m-price">
                    <span class="psos">PHP</span>19.20<span class="unit">/Day</span><br/>
                </p>
                <p class="uk-text-center uk-margin-large-top btn-mob"><a href="javascript:void(0);">view more</a></p>
            </span>
        </h3>
        <div class="uk-accordion-content mobcust-content">
            <p>You want want a healtchare coverage worth P30,000 per illness coverage.</p>
            <p>Free accounting and payoll software.  You want paying bills online.  You want free advice on legal, tax and accounting matters. Discounts on co-working spaces.</p>
            <p>Want to avail of SEC/DTI registration, permits, BIR filings, bookeeping, and others.</p>
             <p>
            <h3>Covers :</h3>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Free Accounting and Payroll Software
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                <p><strong>Free Accounting and Payroll Software</strong></p>
                    Accounting and Payroll ready software is available for use.                                                 
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Managed Healthcare Services
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                <p><strong>Managed Healthcare Services</strong></p>
                    Use your prepaid health card in all Healthway clinics and enjoy discounted 
                    rates on consultations and laboratory tests without annual limits for you and 
                    your family.
                    <br />
                    <br />
                    <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                           
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Health Care Insurance up to PHP 30,000 MBL <br />(Maximum Benefit Limit)
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                <p><strong>Health Care Insurance</strong></p>
                    Members can avail up to PHP 30,000 MBL per sickness with cashless transactions 
                    on accredited hospitals and clinics. Pre-existing condition not covered. 
                    <br />
                    <br /> 
                    <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                      
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Life & Accident Insurance
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                <p><strong>Life & Accident Insurance</strong></p>
                    Get <b>double coverage</b> on life and accident insurance plus <b>FREE</b> access to international medical assistance and emergency services.                                                        
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <!-- <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a> -->
                Travel Insurance (Local & International)
                <!-- <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Filings : Monthly/ Quarterly</strong></p>
                    <ul>
                        <li>1.&nbsp;SSS</li>
                        <li>2.&nbsp;Philhealth</li>
                        <li>3.&nbsp;Pag-ibig</li>
                        <li>4.&nbsp;BIR</li>
                    </ul>
                </div> -->
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Bills Payments
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Bills Payments</strong></p>
                   Pay your utility bills, from water to cable and internet plans, at your convenience! 
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Government Posting
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Government Posting</strong></p>
                   Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Tax & Legal Inquiries
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                <p><strong>Tax & Legal Inquiries</strong></p>
                   Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                </div>
            </div>
        </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Referral Program
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                        <p><strong>Referral Program</strong></p>
                        Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                </div>
            </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Co-working Space
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                <p><strong>Co-working Space</strong></p>
                    Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Business Cards
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                <p><strong>Business Cards</strong></p>
                    Get discounted rates.
                </div>
            </div>
        </p>
        <br />
        <p>
            <div class="uk-width-1-1 price-nest sep-cat">
                Listing Business Compliance Services
           </div>
           <div class="uk-width-1-1 price-info">
                <p><b>PRODUCTS PRICED SEPARATELY</b></p>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Registration
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Registration</strong></p>
                    <ul>
                        <li>1. Virtual Office</li>
                        <li>2. SEC</li>
                        <li>3. DTI</li>
                        <li>4. BUSINESS PERMITS</li>
                        <li>5.  Barangay Clearance</li>
                        <li>
                            6.  BIR: Non-Vat/VAT Registration
                            <ul>
                                <li>a.  OR Printing</li>
                                <li>b.  Invoice OR Variations</li>
                            </ul>
                        </li>
                        <li>7.  SSS: Employer/Employees</li>
                        <li>8.  Pag-ibig: Employer/Employees</li>
                        <li>9.  Philhealth: Employer/Employees</li>
                    </ul>
                </div>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Monthly/ Quarterly Filings
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Filings : Monthly/ Quarterly</strong></p>
                    <ul>
                        <li>1.&nbsp;SSS</li>
                        <li>2.&nbsp;Philhealth</li>
                        <li>3.&nbsp;Pag-ibig</li>
                        <li>4.&nbsp;BIR</li>
                    </ul>
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Annual Filings
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Filings : Annual</strong></p>
                    <ul>
                        <li>1.&nbsp;Audited Financial Statement</li>
                        <li>2.&nbsp;Income Tax Return</li>
                        <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                    </ul>
                </div>
            </div>
        </p>
        <br />
         <p>
            <div class="uk-width-1-1 price-nest sep-cat">
                TOP Hospitals Included
           </div>
           <div class="uk-width-1-1 price-info">
                <p><b>HOSPITALS THAT ARE INCLUDED ON PACKAGES</b></p>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Asian Hospital and Medical Center 
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                Makati Medical Center
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                St Lukes QC
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                The Medical City
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Cardinal Santos Medical Center
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                World Citi Medical Center
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Manila Doctors Hospital
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                UST Hospital
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Reimbursment</strong></p>
                    This Hospital is not accredited but our partners will reimburse the expense.
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                St. Lukes BGC
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Reimbursment</strong></p>
                    This Hospital is not accredited but our partners will reimburse the expense.
                </div>
            </div>
        </p> 
    <div class="uk-width-1-1 uk-margin-large-top">
            <br />
        <div class="uk-width-small-1-1 uk-container-center uk-text-center">
            <a href="http://horsepower.ph/members/register/solo/silver/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
            
            </a>
        </div>
    </div> 
    <div class="uk-width-1-1 uk-margin-large-top">
        <div class="uk-width-small-1-2 uk-container-center uk-text-center">
            <a href="http://horsepower.ph/business-silver/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
            
            </a>
        </div>
    </div> 
        </div>
        
    </div>
    <!-- GOLD HERE -->
    <div class="uk-accordion" data-uk-accordion="">
        <h3 class="uk-accordion-title uk-active cust-title">
            <span class="uk-text-center mob-plans">
                <p>Business Gold</p>
                <br />
                <p class="m-price">
                    <span class="psos">PHP</span>30.30<span class="unit">/Day</span><br/>
                </p>
                <p class="uk-text-center uk-margin-large-top btn-mob"><a href="javascript:void(0);">view more</a></p>
            </span>
        </h3>
        <div class="uk-accordion-content mobcust-content">
            <p>Same benefits as Silver, but with P75,000 healthcare coverage per illness</p>
           <p>
                <h3>Covers :</h3>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Free Accounting and Payroll Software
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Free Accounting and Payroll Software</strong></p>
                        Accounting and Payroll ready software is available for use.                                                  
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Managed Healthcare Services
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Managed Healthcare Services</strong></p>
                        Use your prepaid health card in all Healthway clinics and enjoy discounted 
                        rates on consultations and laboratory tests without annual limits for you and 
                        your family.
                        <br />
                        <br />
                        <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                           
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Health Care Insurance up to PHP 75,000 MBL <br />(Maximum Benefit Limit)
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Health Care Insurance</strong></p>
                        Members can avail <b>up to PHP 75,000 MBL</b> per sickness with 
                        cashless transactions on accredited hospitals and clinics. Pre-existing condition not covered.                                                     
                        <br />
                        <br />
                        <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b> 
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Life & Accident Insurance
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Life & Accident Insurance</strong></p>
                        Get <b>double coverage</b> on life and accident insurance plus <b>FREE</b> access to international medical assistance and emergency services.                                                        
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <!-- <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a> -->
                    Travel Insurance (Local & International)
                    <!-- <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Filings : Monthly/ Quarterly</strong></p>
                        <ul>
                            <li>1.&nbsp;SSS</li>
                            <li>2.&nbsp;Philhealth</li>
                            <li>3.&nbsp;Pag-ibig</li>
                            <li>4.&nbsp;BIR</li>
                        </ul>
                    </div> -->
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Bills Payments
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Bills Payments</strong></p>
                       Pay your utility bills, from water to cable and internet plans, at your convenience! 
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Government Posting
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                       <p><strong>Government Posting</strong></p>
                       Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Tax & Legal Inquiries
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Tax & Legal Inquiries</strong></p>
                       Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                    </div>
                </div>
            </p>
                <p>
                    <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                        <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                        Referral Program
                        <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Referral Program</strong></p>
                            Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                    </div>
                </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Co-working Space
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Co-working Space</strong></p>
                        Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                    </div>
                </div>
            </p>
            <p>
                <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                    <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                    Business Cards
                    <div class="uk-dropdown uk-dropdown-bottom ppricing">
                    <p><strong>Business Cards</strong></p>
                        Get discounted rates.
                    </div>
                </div>
            </p>
        <br />
        <p>
            <div class="uk-width-1-1 price-nest sep-cat">
                Listing Business Compliance Services
           </div>
           <div class="uk-width-1-1 price-info">
                <p><b>PRODUCTS PRICED SEPARATELY</b></p>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Registration
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Registration</strong></p>
                    <ul>
                        <li>1. Virtual Office</li>
                        <li>2. SEC</li>
                        <li>3. DTI</li>
                        <li>4. BUSINESS PERMITS</li>
                        <li>5.  Barangay Clearance</li>
                        <li>
                            6.  BIR: Non-Vat/VAT Registration
                            <ul>
                                <li>a.  OR Printing</li>
                                <li>b.  Invoice OR Variations</li>
                            </ul>
                        </li>
                        <li>7.  SSS: Employer/Employees</li>
                        <li>8.  Pag-ibig: Employer/Employees</li>
                        <li>9.  Philhealth: Employer/Employees</li>
                    </ul>
                </div>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Monthly/ Quarterly Filings
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Filings : Monthly/ Quarterly</strong></p>
                    <ul>
                        <li>1.&nbsp;SSS</li>
                        <li>2.&nbsp;Philhealth</li>
                        <li>3.&nbsp;Pag-ibig</li>
                        <li>4.&nbsp;BIR</li>
                    </ul>
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Annual Filings
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Filings : Annual</strong></p>
                    <ul>
                        <li>1.&nbsp;Audited Financial Statement</li>
                        <li>2.&nbsp;Income Tax Return</li>
                        <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                    </ul>
                </div>
            </div>
        </p>
        <br />
         <p>
            <div class="uk-width-1-1 price-nest sep-cat">
                TOP Hospitals Included
           </div>
           <div class="uk-width-1-1 price-info">
                <p><b>HOSPITALS THAT ARE INCLUDED ON PACKAGES</b></p>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Asian Hospital and Medical Center 
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                Makati Medical Center
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                St Lukes QC
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                The Medical City
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Cardinal Santos Medical Center
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                World Citi Medical Center
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Manila Doctors Hospital
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                UST Hospital
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Reimbursment</strong></p>
                    This Hospital is not accredited but our partners will reimburse the expense.
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                St. Lukes BGC
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Reimbursment</strong></p>
                    This Hospital is not accredited but our partners will reimburse the expense.
                </div>
            </div>
        </p> 
    <div class="uk-width-1-1 uk-margin-large-top">
            <br />
        <div class="uk-width-small-1-1 uk-container-center uk-text-center">
            <a href="http://horsepower.ph/members/register/solo/gold/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
            
            </a>
        </div>
    </div> 
    <div class="uk-width-1-1 uk-margin-large-top">
        <div class="uk-width-small-1-2 uk-container-center uk-text-center">
            <a href="http://horsepower.ph/business-gold/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
            
            </a>
        </div>
    </div> 
        </div>
        
    </div>
    <!-- PLATINUM HERE -->
     <div class="uk-accordion" data-uk-accordion="">
        <h3 class="uk-accordion-title uk-active cust-title">
            <span class="uk-text-center mob-plans">
                <p>Business Platinum</p>
                <br />
                <p class="m-price">
                    <span class="psos">PHP</span>41.40<span class="unit">/Day</span><br/>
                </p>
                <p class="uk-text-center uk-margin-large-top btn-mob"><a href="javascript:void(0);">view more</a></p>
            </span>
        </h3>
        <div class="uk-accordion-content mobcust-content">
            <p>
                Premium membership package with full privilege in online bills payment and government postings, healthcare insurance plan, double coverage life and accident insurance and international medical assistance. Enjoy 25 hours a month co-working space benefit from our selected partners, 
                general tax and legal advice, basic accounting software, retails rewards and referral incentives.                                                   
                </p>
               <p>
                        <h3>Covers :</h3>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Free Accounting and Payroll Software
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Free Accounting and Payroll Software</strong></p>
                                Accounting and Payroll ready software is available for use.                                                    
                            </div>
                        </div>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Managed Healthcare Services
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Managed Healthcare Services</strong></p>
                                Use your prepaid health card in all Healthway clinics and enjoy discounted 
                                rates on consultations and laboratory tests without annual limits for you and 
                                your family.
                                <br />
                                <br />
                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                           
                            </div>
                        </div>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Health Care Insurance up to PHP 100,000 MBL <br />(Maximum Benefit Limit)
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Health Care Insurance</strong></p>
                                Members can avail up to PHP 100,000 MBL per sickness with cashless transactions on accredited hospitals and clinics. 
                                <b>Pre-existing condition not covered</b>.
                                <br />
                                <br />
                                <b>CARD USE TRANSFERRABLE TO DEPENDENTS</b>                                                            
                            </div>
                        </div>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Life & Accident Insurance
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Life & Accident Insurance</strong></p>
                                Get <b>double coverage</b> on life and accident insurance plus <b>FREE</b> access to international medical assistance and emergency services.                                                        
                            </div>
                        </div>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <!-- <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a> -->
                            Travel Insurance (Local & International)
                            <!-- <div class="uk-dropdown uk-dropdown-bottom ppricing">
                               <p><strong>Filings : Monthly/ Quarterly</strong></p>
                                <ul>
                                    <li>1.&nbsp;SSS</li>
                                    <li>2.&nbsp;Philhealth</li>
                                    <li>3.&nbsp;Pag-ibig</li>
                                    <li>4.&nbsp;BIR</li>
                                </ul>
                            </div> -->
                        </div>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Bills Payments
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                               <p><strong>Bills Payments</strong></p>
                               Pay your utility bills, from water to cable and internet plans, at your convenience! 
                            </div>
                        </div>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Government Posting
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                               <p><strong>Government Posting</strong></p>
                               Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again!
                            </div>
                        </div>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Tax & Legal Inquiries
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Tax & Legal Inquiries</strong></p>
                               Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
                            </div>
                        </div>
                    </p>
                        <p>
                            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                                Referral Program
                                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                                    <p><strong>Referral Program</strong></p>
                                    Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience.                                                                </div>
                            </div>
                        </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Co-working Space
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Co-working Space</strong></p>
                                Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms!                                                           
                            </div>
                        </div>
                    </p>
                    <p>
                        <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                            <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                            Business Cards
                            <div class="uk-dropdown uk-dropdown-bottom ppricing">
                            <p><strong>Business Cards</strong></p>
                                Get discounted rates.
                            </div>
                        </div>
                    </p>
        <br />
        <p>
            <div class="uk-width-1-1 price-nest sep-cat">
                Listing Business Compliance Services
           </div>
           <div class="uk-width-1-1 price-info">
                <p><b>PRODUCTS PRICED SEPARATELY</b></p>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Registration
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Registration</strong></p>
                    <ul>
                        <li>1. Virtual Office</li>
                        <li>2. SEC</li>
                        <li>3. DTI</li>
                        <li>4. BUSINESS PERMITS</li>
                        <li>5.  Barangay Clearance</li>
                        <li>
                            6.  BIR: Non-Vat/VAT Registration
                            <ul>
                                <li>a.  OR Printing</li>
                                <li>b.  Invoice OR Variations</li>
                            </ul>
                        </li>
                        <li>7.  SSS: Employer/Employees</li>
                        <li>8.  Pag-ibig: Employer/Employees</li>
                        <li>9.  Philhealth: Employer/Employees</li>
                    </ul>
                </div>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Monthly/ Quarterly Filings
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Filings : Monthly/ Quarterly</strong></p>
                    <ul>
                        <li>1.&nbsp;SSS</li>
                        <li>2.&nbsp;Philhealth</li>
                        <li>3.&nbsp;Pag-ibig</li>
                        <li>4.&nbsp;BIR</li>
                    </ul>
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                Annual Filings
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Filings : Annual</strong></p>
                    <ul>
                        <li>1.&nbsp;Audited Financial Statement</li>
                        <li>2.&nbsp;Income Tax Return</li>
                        <li>3.&nbsp;SEC Reporting AFS and GIS</li>
                    </ul>
                </div>
            </div>
        </p>
        <br />
         <p>
            <div class="uk-width-1-1 price-nest sep-cat">
                TOP Hospitals Included
           </div>
           <div class="uk-width-1-1 price-info">
                <p><b>HOSPITALS THAT ARE INCLUDED ON PACKAGES</b></p>
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Asian Hospital and Medical Center 
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                Makati Medical Center
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                St Lukes QC
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                The Medical City
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Cardinal Santos Medical Center
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                World Citi Medical Center
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                
                Manila Doctors Hospital
            </div>
        </p>
         <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                UST Hospital
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Reimbursment</strong></p>
                    This Hospital is not accredited but our partners will reimburse the expense.
                </div>
            </div>
        </p>
        <p>
            <div class="uk-button-dropdown " data-uk-dropdown="{mode:'hover', pos:'left-top'}" aria-haspopup="true" aria-expanded="true">
                <a href="javascript:void(0);" class="uk-icon-hover uk-icon-question">&nbsp;&nbsp;</a>
                St. Lukes BGC
                <div class="uk-dropdown uk-dropdown-bottom ppricing">
                   <p><strong>Reimbursment</strong></p>
                    This Hospital is not accredited but our partners will reimburse the expense.
                </div>
            </div>
        </p> 
    <div class="uk-width-1-1 uk-margin-large-top">
            <br />
        <div class="uk-width-small-1-1 uk-container-center uk-text-center">
            <a href="http://horsepower.ph/members/register/solo/platinum/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan">GET THIS PLAN <i class="uk-icon-shopping-cart"></i>
            
            </a>
        </div>
    </div> 
    <div class="uk-width-1-1 uk-margin-large-top">
        <div class="uk-width-small-1-2 uk-container-center uk-text-center">
            <a href="http://horsepower.ph/business-platinum/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary lmore-pr">Learn More <i class="uk-icon-angle-double-right"></i>
            
            </a>
        </div>
    </div> 
        </div>
        
    </div>
    </div>
</div>
<br />
<br />
<hr class="uk-divider" />
<div class="uk-width-1-2 uk-container-center uk-margin-large-bottom">
    <a href="http://horsepower.ph/faq/" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan"><i class="uk-icon-info">&nbsp;&nbsp;</i>Visit our FAQ</a>
</div>
</section>

<?php get_footer(); ?>
