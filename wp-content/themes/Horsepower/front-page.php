<?php
if ( 'posts' == get_option( 'show_on_front' ) ) {
	
		get_header(); 

		parallax_one_get_template_part( apply_filters("parallax_one_plus_header_layout","/sections/parallax_one_header_section"));
	?>
		</div>
		<!-- /END COLOR OVER IMAGE -->
	</header>
	<!-- /END HOME / HEADER  -->
				<!-- This is a button toggling the modal -->
	<div itemprop id="content" class="content-warp" role="main">
	        <button class="uk-button" data-uk-modal="{target:'#my-id'}">...</button>
	<?php
		$sections_array = apply_filters("parallax_one_plus_sections_filter",
			array(
				'sections/parallax_one_logos_section',
				'sections/parallax_one_our_services_section',
				'sections/parallax_one_our_story_section',
				'sections/parallax_one_our_team_section',
				'sections/parallax_one_happy_customers_section',
				'sections/parallax_one_ribbon_section',
				'sections/parallax_one_latest_news_section',
				'sections/parallax_one_media_section',
				'sections/parallax_one_contact_info_section',
				'sections/parallax_one_map_section'));

		if(!empty($sections_array)){
			foreach($sections_array as $section){
				parallax_one_get_template_part($section);
			}
		}
	?>

	</div><!-- .content-wrap -->
	<?php 
	get_footer();
} else {

	include( get_page_template() );
}
?>
<!-- <a href="#my-id" class="uk-hidden" data-uk-modal="{center:true}">.</a> -->

<!-- This is a button toggling the modal -->

<!-- This is the modal -->
<!-- <div id="my-id" class="uk-modal" style="display: block;verflow-y: scroll;z-index: 99999;width: 103%;margin: -14px;">
    <div class="uk-modal-dialog" style="width: 100%;height: 100%;margin: 0 15px;z-index: 9999999999999;background: rgba(248, 248, 248, 0.96);">
    <div class="uk-width-small-1-2 uk-container-center uk-text-center">
    	   <h2><b>HORSEPOWER PHOTOCONTEST !</b></h2>
    	   <br />
    	  <a href="http://horsepower.ph/wp-content/uploads/2016/02/Photo-Contest-Rules-for-Horsepower-Feb.pdf">Download Mechanics Here</a>
    <br />
    <br />
    <img src="http://horsepower.ph/wp-content/uploads/2016/02/photosh-1.jpg" alt="Horsepower Photo Contest" style="width: 50%;">
        <br />
        <br />
        <a class="uk-modal-close">Proceed to page</a>
    </div>
  	</div>
</div> -->

//  <script>
// 	$.UIkit.modal('#my-id').show();
// </script>