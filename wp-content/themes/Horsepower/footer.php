	<footer>
	<br />
 <br />
 		<section class="uk-width-1-1 careers-cont uk-margin-large-top">
			<section class="hp-main-cont">
					<div class="uk-grid uk-grid-small hp-headers">
						<div class="uk-width-medium-1-1 uk-width-small-1-1 hp-texts">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-width-small-1 hp-c-flexer">
									<h2 class="uk-text-left uk-margin-bottom-remove uk-padding-remove title-c">
										We are <span data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:400, repeat: true}">HORSEPOWER!</span>
									</h2>		
								</div>
								<div class="uk-width-medium-1-2 uk-width-small-1">
									<h4 class="uk-margin-top">
										Inspiring. Empowering. Rewarding. Fun. These are some of the words people commonly use to describe their careers at Horsepower!
									</h4>	
									<div class="uk-width-1-1">
										<br />
											<a href="http://horsepower.ph/category/job-posting/" title="" class="uk-button hp-header-btns hp-rstory uk-margin-remove">Join our team!</a>
										<br />
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="uk-grid career-title">
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/engineering/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Engineering
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/design/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Design
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/sales-and-marketing/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Sales and Marketing (Community, Field Sales, Affiliate, Digital Marketing)
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/product-and-business-development/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Product and Business Development
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="#" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Operations and Finance
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/social-services/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Social (Client services, PR and Media relations)
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
					</div>
			</section>
		</section>
		<div class="hp-footer">
            <div class="uk-container uk-container-center uk-text-center">
                <ul class="uk-subnav uk-margin-bottom-remove uk-subnav-line uk-flex-center">
                   <!--  <li><a href="">Careers</a></li> -->
                    <li><a href="http://horsepower.ph/solo-entrepreneur/">Products</a></li>
                    <li><a href="http://horsepower.ph/faq/">FAQ</a></li>
                    <li><a href="http://horsepower.ph/about-us/">About Us</a></li>
                </ul>
                <div class="uk-panel">
                    <p>All rights reserved <a href="http://horsepower.ph" style="color: #FB1D26;">Horsepower.ph</a> &copy;2016</p>
                </div>
                 <ul class="uk-subnav uk-flex-center">
                    <li><a href="https://goo.gl/csI9Zj"><a href="#" title=""><i class="uk-icon uk-icon-facebook"></i></a></a></li>
                    <li><a href="https://twitter.com/horsepowerph"><i class="uk-icon uk-icon-twitter"></i></a></li>
                    <li><a href="https://goo.gl/i011Wh" title=""><i class="uk-icon uk-icon-linkedin"></i></a></li>
                    <li><a href="https://www.instagram.com/teamhorsepowerph/" title=""><i class="uk-icon uk-icon-instagram"></i></a></li>
                    <li><a href="http://www.slideshare.net/HorsepowerPH" title=""><i class="uk-icon uk-icon-slideshare"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCLg9Zb2aHaOn0XfV9bdu0eA" title=""><i class="uk-icon uk-icon-youtube"></i></a></li>
                    <li><a href="#" title=""><i class="uk-icon uk-icon-google-plus"></i></a></li>

                </ul>

            </div>
        </div>
	</footer>
	<script>
		function initialize() {
		  var mapCanvas = document.getElementById('map-canvas');
		  var mapOptions = {
		    center: new google.maps.LatLng(14.585197, 121.062046),
		    zoom: 16,
		    mapTypeId: google.maps.MapTypeId.ROADMAP
		  }
		  var map = new google.maps.Map(mapCanvas, mapOptions);
		  var Latlng_0 = new google.maps.LatLng(14.585197, 121.062046);
	      var marker_0 = new google.maps.Marker(
	        {
	            position: Latlng_0, 
	            title:"0"
	        }
	    );
	    marker_0.setMap(map);
		}

		google.maps.event.addDomListener(window, 'load', initialize);
		
		// function moveScroll(){
		//     var scroll = $(window).scrollTop();
		//     var anchor_top = $("#maintable").offset().top;
		//     var anchor_bottom = $("#bottom_anchor").offset().top;
		//     if (scroll>anchor_top && scroll<anchor_bottom) {
		//     clone_table = $("#clone");
		//     if(clone_table.length == 0){
		//         clone_table = $("#maintable").clone();
		//         clone_table.attr('id', 'clone');
		//         clone_table.css({position:'fixed',
		//                  'pointer-events': 'none',
		//                  top:-15});
		//         clone_table.width($("#maintable").width());
		//         $("#table-container").append(clone_table);
		//         $("#clone").css({visibility:'hidden'});
		//         $("#clone thead").css({visibility:'visible'});
		//     }
		//     } else {
		//     $("#clone").remove();
		//     }
		// }
		// $(window).scroll(moveScroll);
		
	</script>
<script type='text/javascript'>var fc_CSS=document.createElement('link');fc_CSS.setAttribute('rel','stylesheet');var isSecured = (window.location && window.location.protocol == 'https:');var rtlSuffix = ((document.getElementsByTagName('html')[0].getAttribute('lang')) === 'ar') ? '-rtl' : '';fc_CSS.setAttribute('type','text/css');fc_CSS.setAttribute('href',((isSecured)? 'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets1.chat.freshdesk.com')+'/css/visitor'+rtlSuffix+'.css');document.getElementsByTagName('head')[0].appendChild(fc_CSS);var fc_JS=document.createElement('script'); fc_JS.type='text/javascript';fc_JS.src=((isSecured)?'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets.chat.freshdesk.com')+'/js/visitor.js';(document.body?document.body:document.getElementsByTagName('head')[0]).appendChild(fc_JS);window.freshchat_setting= 'eyJ3aWRnZXRfc2l0ZV91cmwiOiJpbnF1aXJpZXMuaG9yc2Vwb3dlci5waCIsInByb2R1Y3RfaWQiOjUwMDAwMDUzMTksIm5hbWUiOiJHZW5lcmFsIElucXVpcmllcyBhbmQgU3VwcG9ydCIsIndpZGdldF9leHRlcm5hbF9pZCI6NTAwMDAwNTMxOSwid2lkZ2V0X2lkIjoiMmQzMDIzZWMtNGJkNy00ZmYxLTk5YzMtMmI3OWU1NzhjMjBiIiwic2hvd19vbl9wb3J0YWwiOnRydWUsInBvcnRhbF9sb2dpbl9yZXF1aXJlZCI6ZmFsc2UsImlkIjo1MDAwMDgwNjU0LCJtYWluX3dpZGdldCI6ZmFsc2UsImZjX2lkIjoiYzMxMmNmNDAzMThjMjgzMTlkYTliMDk4NWNlMTA2YmQiLCJzaG93IjoxLCJyZXF1aXJlZCI6MiwiaGVscGRlc2tuYW1lIjoiSG9yc2Vwb3dlci5waCIsIm5hbWVfbGFiZWwiOiJOYW1lIiwibWFpbF9sYWJlbCI6IkVtYWlsIiwibWVzc2FnZV9sYWJlbCI6Ik1lc3NhZ2UiLCJwaG9uZV9sYWJlbCI6IlBob25lIE51bWJlciIsInRleHRmaWVsZF9sYWJlbCI6IlRleHRmaWVsZCIsImRyb3Bkb3duX2xhYmVsIjoiRHJvcGRvd24iLCJ3ZWJ1cmwiOiJob3JzZXBvd2VyLmZyZXNoZGVzay5jb20iLCJub2RldXJsIjoiY2hhdC5mcmVzaGRlc2suY29tIiwiZGVidWciOjEsIm1lIjoiTWUiLCJleHBpcnkiOjAsImVudmlyb25tZW50IjoicHJvZHVjdGlvbiIsImRlZmF1bHRfd2luZG93X29mZnNldCI6MzAsImRlZmF1bHRfbWF4aW1pemVkX3RpdGxlIjoiQ2hhdCBpbiBwcm9ncmVzcyIsImRlZmF1bHRfbWluaW1pemVkX3RpdGxlIjoiTGV0J3MgdGFsayEiLCJkZWZhdWx0X3RleHRfcGxhY2UiOiJZb3VyIE1lc3NhZ2UiLCJkZWZhdWx0X2Nvbm5lY3RpbmdfbXNnIjoiV2FpdGluZyBmb3IgYW4gYWdlbnQiLCJkZWZhdWx0X3dlbGNvbWVfbWVzc2FnZSI6IkhpISBIb3cgY2FuIHdlIGhlbHAgeW91IHRvZGF5PyIsImRlZmF1bHRfd2FpdF9tZXNzYWdlIjoiT25lIG9mIHVzIHdpbGwgYmUgd2l0aCB5b3UgcmlnaHQgYXdheSwgcGxlYXNlIHdhaXQuIiwiZGVmYXVsdF9hZ2VudF9qb2luZWRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGpvaW5lZCB0aGUgY2hhdCIsImRlZmF1bHRfYWdlbnRfbGVmdF9tc2ciOiJ7e2FnZW50X25hbWV9fSBoYXMgbGVmdCB0aGUgY2hhdCIsImRlZmF1bHRfYWdlbnRfdHJhbnNmZXJfbXNnX3RvX3Zpc2l0b3IiOiJZb3VyIGNoYXQgaGFzIGJlZW4gdHJhbnNmZXJyZWQgdG8ge3thZ2VudF9uYW1lfX0iLCJkZWZhdWx0X3RoYW5rX21lc3NhZ2UiOiJUaGFuayB5b3UgZm9yIGNoYXR0aW5nIHdpdGggdXMuIElmIHlvdSBoYXZlIGFkZGl0aW9uYWwgcXVlc3Rpb25zLCBmZWVsIGZyZWUgdG8gcGluZyB1cyEiLCJkZWZhdWx0X25vbl9hdmFpbGFiaWxpdHlfbWVzc2FnZSI6Ik91ciBhZ2VudHMgYXJlIHVuYXZhaWxhYmxlIHJpZ2h0IG5vdy4gU29ycnkgYWJvdXQgdGhhdCwgYnV0IHBsZWFzZSBsZWF2ZSB1cyBhIG1lc3NhZ2UgYW5kIHdlJ2xsIGdldCByaWdodCBiYWNrLiIsImRlZmF1bHRfcHJlY2hhdF9tZXNzYWdlIjoiV2UgY2FuJ3Qgd2FpdCB0byB0YWxrIHRvIHlvdS4gQnV0IGZpcnN0LCBwbGVhc2UgdGVsbCB1cyBhIGJpdCBhYm91dCB5b3Vyc2VsZi4iLCJhZ2VudF90cmFuc2ZlcmVkX21zZyI6IllvdXIgY2hhdCBoYXMgYmVlbiB0cmFuc2ZlcnJlZCB0byB7e2FnZW50X25hbWV9fSIsImFnZW50X3Jlb3Blbl9jaGF0X21zZyI6Int7YWdlbnRfbmFtZX19IHJlb3BlbmVkIHRoZSBjaGF0IiwidmlzaXRvcl9zaWRlX2luYWN0aXZlX21zZyI6IlRoaXMgY2hhdCBoYXMgYmVlbiBpbmFjdGl2ZSBmb3IgdGhlIHBhc3QgMjAgbWludXRlcy4iLCJhZ2VudF9kaXNjb25uZWN0X21zZyI6Int7YWdlbnRfbmFtZX19IGhhcyBiZWVuIGRpc2Nvbm5lY3RlZCIsInNpdGVfaWQiOiJjMzEyY2Y0MDMxOGMyODMxOWRhOWIwOTg1Y2UxMDZiZCIsImFjdGl2ZSI6dHJ1ZSwid2lkZ2V0X3ByZWZlcmVuY2VzIjp7IndpbmRvd19jb2xvciI6IiNkNzAwMDAiLCJ3aW5kb3dfcG9zaXRpb24iOiJCb3R0b20gUmlnaHQiLCJ3aW5kb3dfb2Zmc2V0IjoiMzAiLCJ0ZXh0X3BsYWNlIjoiWW91ciBNZXNzYWdlIiwiY29ubmVjdGluZ19tc2ciOiJXYWl0aW5nIGZvciBhbiBhZ2VudCIsImFnZW50X2xlZnRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGxlZnQgdGhlIGNoYXQiLCJhZ2VudF9qb2luZWRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGpvaW5lZCB0aGUgY2hhdCIsIm1pbmltaXplZF90aXRsZSI6IkxldCdzIHRhbGshIiwibWF4aW1pemVkX3RpdGxlIjoiQ2hhdCBpbiBwcm9ncmVzcyIsIndlbGNvbWVfbWVzc2FnZSI6IkhpISBIb3cgY2FuIHdlIGhlbHAgeW91IHRvZGF5PyIsInRoYW5rX21lc3NhZ2UiOiJUaGFuayB5b3UgZm9yIGNoYXR0aW5nIHdpdGggdXMuIElmIHlvdSBoYXZlIGFkZGl0aW9uYWwgcXVlc3Rpb25zLCBmZWVsIGZyZWUgdG8gcGluZyB1cyEiLCJ3YWl0X21lc3NhZ2UiOiJPbmUgb2YgdXMgd2lsbCBiZSB3aXRoIHlvdSByaWdodCBhd2F5LCBwbGVhc2Ugd2FpdC4ifSwicm91dGluZyI6eyJkcm9wZG93bl9iYXNlZCI6ImZhbHNlIiwiY2hvaWNlcyI6eyJsaXN0MSI6WyIwIl0sImxpc3QyIjpbIjAiXSwibGlzdDMiOlsiMCJdLCJkZWZhdWx0IjpbIjAiXX19LCJwcmVjaGF0X2Zvcm0iOnRydWUsInByZWNoYXRfbWVzc2FnZSI6IldlIGNhbid0IHdhaXQgdG8gdGFsayB0byB5b3UuIEJ1dCBmaXJzdCwgcGxlYXNlIHRha2UgYSBjb3VwbGUgb2YgbW9tZW50cyB0byB0ZWxsIHVzIGEgYml0IGFib3V0IHlvdXJzZWxmLiIsInByZWNoYXRfZmllbGRzIjp7Im5hbWUiOnsidGl0bGUiOiJOYW1lIiwic2hvdyI6IjIifSwiZW1haWwiOnsidGl0bGUiOiJFbWFpbCIsInNob3ciOiIyIn0sInBob25lIjp7InRpdGxlIjoiUGhvbmUgTnVtYmVyIiwic2hvdyI6IjEifSwidGV4dGZpZWxkIjp7InRpdGxlIjoiVGV4dGZpZWxkIiwic2hvdyI6IjIifSwiZHJvcGRvd24iOnsidGl0bGUiOiJEcm9wZG93biIsIm9wdGlvbnMiOlsibGlzdDEiLCJsaXN0MiIsImxpc3QzIl19fSwiYnVzaW5lc3NfY2FsZW5kYXIiOm51bGwsIm5vbl9hdmFpbGFiaWxpdHlfbWVzc2FnZSI6eyJ0ZXh0IjoiT3VyIGFnZW50cyBhcmUgdW5hdmFpbGFibGUgcmlnaHQgbm93LiBTb3JyeSBhYm91dCB0aGF0LCBidXQgcGxlYXNlIGxlYXZlIHVzIGEgbWVzc2FnZSBhbmQgd2UnbGwgZ2V0IHJpZ2h0IGJhY2suIiwiY3VzdG9tTGluayI6IjAiLCJjdXN0b21MaW5rVXJsIjoiIn0sInByb2FjdGl2ZV9jaGF0Ijp0cnVlLCJwcm9hY3RpdmVfdGltZSI6MTA1LCJzaXRlX3VybCI6ImlucXVpcmllcy5ob3JzZXBvd2VyLnBoIiwiZXh0ZXJuYWxfaWQiOjUwMDAwMDUzMTksImRlbGV0ZWQiOmZhbHNlLCJvZmZsaW5lX2NoYXQiOnsic2hvdyI6IjEiLCJmb3JtIjp7Im5hbWUiOiJOYW1lIiwiZW1haWwiOiJFbWFpbCIsIm1lc3NhZ2UiOiJNZXNzYWdlIn0sIm1lc3NhZ2VzIjp7InRpdGxlIjoiTGVhdmUgdXMgYSBtZXNzYWdlISIsInRoYW5rIjoiVGhhbmsgeW91IGZvciB3cml0aW5nIHRvIHVzLiBXZSB3aWxsIGdldCBiYWNrIHRvIHlvdSBzaG9ydGx5LiIsInRoYW5rX2hlYWRlciI6IlRoYW5rIHlvdSEifX0sIm1vYmlsZSI6dHJ1ZSwiY3JlYXRlZF9hdCI6IjIwMTUtMDUtMThUMDM6MTQ6MDIuMDAwWiIsInVwZGF0ZWRfYXQiOiIyMDE1LTA3LTIzVDE3OjIzOjM5LjAwMFoifQ==';</script>
<?php wp_footer(); ?>
</body>
</html>