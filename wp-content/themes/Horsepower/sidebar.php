<style type="text/css" media="screen">
	section.press.uk-grid .uk-width-small-1-1{
		max-height: 500px;
	}
/*buttons*/
	section.press .uk-grid .uk-width-small-1-1 a{
		margin: 15px 0;
	}
	section.press .uk-grid .uk-width-small-1-1:first-child i:first-child{
		color: #3a5795;
    	text-shadow: none;
	}
	section.press .uk-grid .uk-width-small-1-1:nth-child(2) i:first-child{
		color: #894F39;
    	text-shadow: none;
	}
	section.press .uk-grid .uk-width-small-1-1:nth-child(3) i:first-child{
		color: #666666;
    	text-shadow: none;
	}
/*icons*/
	.press-btn{
	    border: 1px solid rgba(158, 8, 8, 0.68);
	    padding: 10px;
	    border-radius: 4px;

	}
	.press-btn:hover{
	    text-decoration: none;
	    
	}
	.timeline-header.customisable-border {
		width: 100%; background: red;
	}
	.press h4{
		color: #fff;
		text-transform: uppercase;
	    padding: 10px 0;
		border-bottom: 1px dashed #eee;
	}
</style>
<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package parallax-one
 */



if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div itemscope itemtype="http://schema.org/WPSideBar" role="complementary" aria-label="<?php esc_html_e('Main sidebar','parallax-one')?>" id="sidebar-secondary" class="uk-width-small-1-1 widget-area">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>

<section class="press">
	<h4>Follow us on</h4>
		<div class="uk-grid uk-grid-small">	
			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a class="press uk-icon-button">
                    	<i class="uk-icon uk-icon-facebook"></i>
                	</a>
                    <div class="uk-dropdown uk-dropdown-large uk-dropdown-left drp">
                        <ul class="uk-nav uk-nav-dropdown">
                            <li>
                            	<?php echo do_shortcode("[facebook_likebox]"); ?>
                            </li>
                        </ul>
                    </div>
                </div>
			</div>
			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a class="press uk-icon-button">
                    	<i class="uk-icon uk-icon-instagram" style="color:#894F39;"></i>
                	</a>
                    <div class="uk-dropdown uk-dropdown-large uk-dropdown-left drp">
                        <ul class="uk-nav uk-nav-dropdown">
                            <li>
								<?php echo do_shortcode("[instagram-feed]"); ?>
                            </li>
                        </ul>
                    </div>
                </div>
			</div>
			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a class="press uk-icon-button">
                    	<i class="uk-icon uk-icon-linkedin"></i>
                	</a>
                    <div class="uk-dropdown uk-dropdown-large uk-dropdown-left drp">
                        <ul class="uk-nav uk-nav-dropdown">
                            <li>
								<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
								<script type="IN/CompanyProfile" data-id="4847866" data-format="inline"></script>
                            </li>
                        </ul>
                    </div>
                </div>	
			</div>

			<!-- other medias -->

			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a href="https://www.youtube.com/channel/UCLg9Zb2aHaOn0XfV9bdu0eA" class="press uk-icon-button">
                    	<i class="uk-icon uk-icon-youtube" style="color:red"></i>
                	</a>
                </div>
			</div>
			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a href="http://www.slideshare.net/HorsepowerPH" class="press uk-icon-button">
                    	<i class="uk-icon uk-icon-slideshare" style="color:#894F39;"></i>
                	</a>
                </div>
			</div>
			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a class="press uk-icon-button">
                    	<i class="uk-icon uk-icon-google-plus"></i>
                	</a>
                </div>	
			</div>
	</div>
</section>
<hr style="background:rgba(204, 204, 204, 0.13);" />
	<div class="uk-width-small-1-1">
	<?php echo do_shortcode("[TWTR]"); ?>
	</div>
</div><!-- #sidebar-secondary -->
