<?php
/**
 * The template for displaying all single posts.
 *
 * @package parallax-one
 */

	get_header(); 
?>

	</div>
	<!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->

<div class="content-wrap">
	<div class="container">

		<div class="uk-grid uk-grid-small uk-margin-large-top">
			<div class="uk-width-medium-7-10 uk-width-small-1-1">
				<div id="primary" class="content-area <?php if ( is_active_sidebar( 'sidebar-1' ) ) { echo 'col-md-12';} else {echo 'col-md-12';}  ?>">
					<main itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage" id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'single' ); ?>
						
						<?php //the_post_navigation();  ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>

					<?php endwhile; // end of the loop. ?>

					</main><!-- #main -->
				</div><!-- #primary -->
			</div>
			<div class="uk-width-medium-3-10 uk-width-small-1-1">
				<?php get_sidebar(); ?>
			</div>
		</div>
		

	</div>
</div><!-- .content-wrap -->

<?php get_footer(); ?>

