<?php
	$post = $wp_query->post;
	if (in_category('design')) {
		include(TEMPLATEPATH . '/careers-design-single.php');
		} 
	elseif (in_category('Engineering')) {
		include(TEMPLATEPATH . '/career-engineering-single.php');
		} 
	elseif (in_category('sales-and-marketing')) {
		include(TEMPLATEPATH . '/career-marketing-single.php');
		} 
	elseif (in_category('product-and-business-development')) {
		include(TEMPLATEPATH . '/career-prodbusi-single.php');
		} 
	elseif (in_category('operations-and-finance')) {
		include(TEMPLATEPATH . '/career-opsfinance-single.php');
		} 
	elseif (in_category('social-services')) {
		include(TEMPLATEPATH . '/career-social-single.php');
		} 
	else {
		include(TEMPLATEPATH . '/normal-single.php');
	}
?>