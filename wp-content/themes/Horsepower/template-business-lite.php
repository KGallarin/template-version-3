<?php
/**
 * Template name: Business Lite
 *
 * @package parallax-one
 */

	get_header(); 
?>

	</div>
	<!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->

<div class="hp-main-cont">
		<div class="uk-grid uk-grid-small">
			<div class="uk-width-small-1-1 uk-width-medium-3-10 hp-img-cont">
				<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/lite-plan.png" title="Horsepower" alt="Horsepower">
			</div>
			<div class="uk-width-small-1-1 uk-width-medium-7-10 hp-cont-cont">
					<div class="content">
						<div class="uk-clearfix">
							<div class="uk-float-left">
								<p class="title uk-margin-bottom-remove">
									<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/busilite.png" title="Horsepower" alt="Horsepower">
									BUSINESS LITE
								</p>
							</div>
							<div class="uk-float-right">
								<p class="uk-text-right">
									<a href="http://horsepower.ph/members/register/solo/lite/" class="uk-button uk-button-primary hp-get"><i class="fa fa-shopping-cart">&nbsp;&nbsp;</i>GET THIS PLAN</a>
								</p>
							</div>
						</div>
						<p class="description">
							Entry level membership with full privilege in online bills payment and government postings, general tax and legal support and referral incentives.						</p>
					</div>
				<br />
			</div>
			<div class="uk-width-small-1-1 uk-width-medium-1-1">
				<div class="hp-text-btm">
					<div class="hr-re">
						<span class="c-text">Membership Plan Benefits</span>
					</div>
				</div>
					<div class="uk-grid hp-ftures">
						<!-- <div class="uk-width-medium-1-2 hp-two-cols">
							<div class="uk-grid uk-grid-small">
								<div class="uk-width-medium-3-10 uk-text-center">
									<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/healthcare.png" title="Horsepower" alt="Horsepower" width="100">
								</div>
								<div class="uk-width-medium-7-10 uk-width-small-1-1">
									Members can avail up to PHP 100,000 MBL per sickness with cashless transactions on accredited hospitals and clinics.
								</div>
							</div>
						</div>
						<div class="uk-width-medium-1-2 hp-two-cols">
							<div class="uk-grid uk-grid-small">
								<div class="uk-width-medium-3-10 uk-text-center">
									<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/insurance.png" title="Horsepower" alt="Horsepower" width="97">
								</div>
								<div class="uk-width-medium-7-10 uk-width-small-1-1">
									Get double coverage on life and accident insurance plus access to international medical assistance and emergency services. Pre-existing condition not covered. 
								</div>
							</div>
						</div> -->

						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/bills-payment.png" title="Horsepower" alt="Horsepower"  style="width: 24%;">
								<div class="uk-text-center icon-title">
									utility bills
								</div>
							</p>
							<p>
								Pay your utility bills, from water to cable and internet plans, at your convenience! Here's the <a href="#hp-modal" title="" data-uk-modal="{center:true}">list</a>.					
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/benefits.png" title="Horsepower" alt="Horsepower"  style="width: 19%;">
								<div class="uk-text-center icon-title">
									Online Government Posting
								</div>
							</p>
							<p>
								Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again! Here's the <a href="#hp-modal-gp" title="" data-uk-modal="{center:true}">list</a>.
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/co-working.png" title="Horsepower" alt="Horsepower"  style="width: 42%;">
								<div class="uk-text-center icon-title">
									Co-working Space
								</div>
							</p>
							<p>
								Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms! Here's our <a href="#hp-modal-cw" title="" data-uk-modal="{center:true}">Office Space Partners</a>
							</p>
						</div>
						            <div id="hp-modal-cw" class="uk-modal">
						                <div class="uk-modal-dialog">
						                <p class="uk-text-right">
						                    <a class="uk-modal-close uk-close uk-text-right"></a>                    
						                </p>
						                    <div class="uk-width-1-1 uk-text-center">
						                        <div class="hp-text-btm uk-margin-top-remove">
						                        <div class="hr-re">
						                            <span class="c-text">Co-working Spaces</span>
						                        </div>
						                    </div>
						                    </div>
						                    <div class="uk-grid hp-bills">
						                        <div class="uk-width-medium-1-3">
						                            <div class="bcont-img">
														<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/impact1.jpg" title="Horsepower" alt="Horsepower"  style="width: 75%;padding: 33px 0;">
						                            </div>
						                            <hr class="uk-divider">
						                        </div>
							                    <div class="uk-width-medium-1-3">
							                            <div class="bcont-img">
														<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/accelerate1.jpg" title="Horsepower" alt="Horsepower"  style="width: 74%;padding: 45px 0;">
							                            </div>
							                            <hr class="uk-divider">
							                    </div>
							                    <div class="uk-width-medium-1-3">
							                            <div class="bcont-img">
															<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/kmc1.jpg" title="Horsepower" alt="Horsepower"  style="width: 95%;padding: 8px 0;">
							                            </div>
							                            <hr class="uk-divider">
							                    </div>
							                    <div class="uk-width-medium-1-3">
						                            <div class="bcont-img">
														<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/47east1.jpg" title="Horsepower" alt="Horsepower"  style="width: 85%;padding: 10px 0;">
						                            </div>
						                            <hr class="uk-divider">
						                        </div>
							                    <div class="uk-width-medium-1-3">
							                            <div class="bcont-img">
														<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/dojo1.jpg" title="Horsepower" alt="Horsepower"  style="width: 61%;padding: 13px 0px;">
							                            </div>
							                            <hr class="uk-divider">
							                    </div>
							                    <div class="uk-width-medium-1-3">
							                            <div class="bcont-img">
															<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/rrc1.jpg" title="Horsepower" alt="Horsepower"  style="width: 67%;padding: 8px 0;">
							                            </div>
							                            <hr class="uk-divider">
							                    </div>
							                </div>
							            </div>
							        </div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/perks.png" title="Horsepower" alt="Horsepower"  style="width: 17%;">
								<div class="uk-text-center icon-title">
									Retail Rewards
								</div>
							</p>
							<p>
								<ul>
									<li>Lifestyle Rewards Program</li>
									<li>Prepaid Master Card</li>
								</ul>
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/taxlegal.jpg" title="Horsepower" alt="Horsepower"  style="width: 38%;">
								<div class="uk-text-center icon-title">
									Tax & Legal Support
								</div>
							</p>
							<p>
								Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/payroll.png" title="Horsepower" alt="Horsepower" style="padding: 19px 0;">
								<div class="uk-text-center icon-title">
									Online Payroll & Accounting System
								</div>
							</p>
							<p>
								Manage your team's compensation packages through our online payroll system. Generate salary computations and adjustments in one click of a button!
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/referral.png" title="Horsepower" alt="Horsepower" style="width: 29%;">
								<div class="uk-text-center icon-title">
									Referral Program
								</div>
							</p>
							<p>
								Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience. <a href="http://horsepower.ph/referal/form" title="">REFER NOW</a>
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/busicard.png" title="Horsepower" alt="Horsepower" style="width: 29%;">
								<div class="uk-text-center icon-title">
									Business Cards
								</div>
							</p>
							<p>
								Print your initial set of business cards on us!
							</p>
						</div>
					</div>
			</div>
			<div class="uk-width-1-1">
			<br /><br /><br /><br />
				<p class="uk-text-center">
					<a href="http://horsepower.ph/members/register/solo/lite/" class="uk-button uk-button-primary hp-get"><i class="fa fa-shopping-cart">&nbsp;&nbsp;</i>GET THIS PLAN</a>
				</p>
			</div>		
		</div>
	            <!-- This is the modal -->
            <div id="hp-modal" class="uk-modal">
                <div class="uk-modal-dialog">
                <p class="uk-text-right">
                    <a class="uk-modal-close uk-close uk-text-right"></a>                    
                </p>
                    <div class="uk-width-1-1 uk-text-center">
                        <div class="hp-text-btm uk-margin-top-remove">
                        <div class="hr-re">
                            <span class="c-text">Pay your hp-bills on</span>
                        </div>
                    </div>
                    </div>
                    <div class="uk-grid hp-bills">
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/bayantel.png" title="Horsepower" alt="Horsepower">
                            </div>
                            <hr class="uk-divider">
                            <div class="hp-bills-cont">
                                 - Bayan DSL
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
							<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/Digitel_Logo.png" title="Horsepower" alt="Horsepower" style="padding: 2px 0;">
                            </div>
                            <hr class="uk-divider">
                            <div class="hp-bills-cont">
                                 - Digitel
                                 <br />
                                 - Digitel DSL Plus
                                 <br />
                                 - Digitel Business DSL
                            </div>
                        </div>
                         <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
							<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/globe1.png" title="Horsepower" alt="Horsepower" style="padding: 6px 0px;width: 78%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="hp-bills-cont">
                                 - Globe Lines
                                 <br />
                                 - Globe Broadband
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/maynilad.png" title="Horsepower" alt="Horsepower" style="padding: 8px 0px;width: 81%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="hp-bills-cont">
                                 - Maynilad
                            </div>
                        </div>
                        <div class="uk-width-1-1 also uk-text-center">
                            <br />
                        </div>
                         <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
                      		<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pldt.png" title="Horsepower" alt="Horsepower" style="padding: 8px 0px;width: 81%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="hp-bills-cont">
                                 - PLDT
                                 <br />
                                 - PLDT MyDSL
                            </div>
                        </div>
                         <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
                      		<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/sky.png" title="Horsepower" alt="Horsepower" style="padding: 13px 0px;width: 80%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="hp-bills-cont">
                                 - Sky Cable
                                 <br />
                                 - Sky Internet
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
                      		<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/smart.jpg" title="Horsepower" alt="Horsepower" style="padding: 4px 0px;width: 79%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="hp-bills-cont">
                                 - Smart
                                 <br />
                                 - Smart Broadband
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
          						<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/sun.png" title="Horsepower" alt="Horsepower" style="padding: 0px 0px;width: 71%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="hp-bills-cont">
                                 - Sun Broadband
                                 <br />
                                 - Sun Cellular
                                 <br />
                                 - Sun Cable System
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- GP Modal -->

            <div id="hp-modal-gp" class="uk-modal">
                <div class="uk-modal-dialog">
                <p class="uk-text-right">
                    <a class="uk-modal-close uk-close uk-text-right"></a>                    
                </p>
                    <div class="uk-width-1-1 uk-text-center">
                        <div class="hp-text-btm uk-margin-top-remove">
                        <div class="hr-re">
                            <span class="c-text">Government Postings</span>
                        </div>
                    </div>
                    </div>
                    <div class="uk-grid hp-bills">
                    <div class="uk-width-medium-1-3">
                            <div class="bcont-img">
          						<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pagibig1.jpg" title="Horsepower" alt="Horsepower" style="width: 74%;">
                            </div>
                            <hr class="uk-divider">
                    </div>
                    <div class="uk-width-medium-1-3">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/sss.png" title="Horsepower" alt="Horsepower"style="width: 99%;padding: 18px 0px;">
                            </div>
                            <hr class="uk-divider">
                        </div>
                    <div class="uk-width-medium-1-3">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/philhealth1.jpg" title="Horsepower" alt="Horsepower" style="width: 95%;padding: 30px 0;">
                            </div>
                            <hr class="uk-divider">
                    </div>
                </div>
            </div>
        </div>
    </div>
<br />
<br />
<?php get_footer(); ?>


