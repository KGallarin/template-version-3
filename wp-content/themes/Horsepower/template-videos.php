<?php 
/*
Template Name: Videos
*/

    get_header(); 
?>

    </div>
    <!-- /END COLOR OVER IMAGE -->
</header>



<section class="container">
<div class="title uk-width-1-1">
        <h2>Press&nbsp;&nbsp;<i class="uk-icon uk-icon-rss-square"></i></h2> 
</div>
    <div class="uk-grid uk-grid-small">
            <div class="uk-width-medium-7-10">
        <div class="uk-grid uk-grid-small">
            <div class="uk-width-small-1-2">
                <?php

                $parallax_number_of_posts = get_option('posts_per_page');
                $args = 
                    array( 
                        'post_type' => 'post', 
                        'category_name' => 'videos',
                        'posts_per_page' => $parallax_number_of_posts, 
                        'order' => 'DESC',
                        'category'=>'updates',
                        'ignore_sticky_posts' => true 
                        );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) {
                    $parallax_one_latest_news_title = get_theme_mod('parallax_one_latest_news_title',esc_html__('Latest news','parallax-one'));
                    if($parallax_number_of_posts > 0) {
                    ?>
                <section class="press uk-width-small-1-1">
                    <?php 
                        $i_latest_posts= 0;
                            while ($the_query->have_posts() ) :  $the_query->the_post();

                                $i_latest_posts++;

                                // if ( !wp_is_mobile() ){
                                //     if($i_latest_posts % 2 == 1){
                                //         echo '<li>';
                                //     }
                                // } else  {
                                //     echo '<li>';
                                // }
                    ?>

                            <div itemscope itemprop="blogPosts" itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" class="timeline-box-wrap" title="<?php printf( esc_html__( 'Latest News: %s', 'parallax-one' ), get_the_title() ) ?>">  
                                <header class="entry-header">
                                    <h1 itemprop="headline" class="entry-title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h1>
                                </header>
                                <div itemprop="description" class="entry-content entry-summary">
                                    <span> <?php the_excerpt(); ?></span>
                                    <span><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more"><?php printf( esc_html__( 'Read more %s', 'textdomain' ), '<span class="screen-reader-text">  '.get_the_title().'</span>' ); ?><i class="uk-icon-angle-double-right"></i></a></span>
                                </div>
                                <span class="entry-date">
                                    <a href="<?php echo esc_url( get_day_link(get_the_date('Y'), get_the_date('m'), get_the_date('d')) ) ?>" rel="bookmark">
                                        <time itemprop="datePublished" datetime="<?php the_time( 'Y-m-d\TH:i:sP' ); ?>" title="<?php the_time( _x( 'l, F j, Y, g:i a', 'post time format', 'parallax-one' ) ); ?>" class="entry-date entry-published updated"><?php echo get_the_date('F j, Y'); ?></time>
                                    </a>
                                </span>
                                
                            </div>

                        <?php
                        if ( !wp_is_mobile() ){
                            if($i_latest_posts % 4 == 0){
                                echo '</li>';
                            }
                        } else {
                            echo '</li>';
                        }

                    endwhile;
                    wp_reset_postdata(); 
                    ?>
                </section>
                <?php
                    }
                } ?>
            </div>
            <div id="primary" class="content-area <?php if ( is_active_sidebar( 'sidebar-1' ) ) { echo 'uk-width-small-1-2';} else {echo 'uk-width-small-1-2';}  ?>">
                    <main itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage" id="main" class="site-main" role="main">
                  <?php echo do_shortcode("[wp_rss_retriever url='http://horsepower.ph/blog/' items='' excerpt='50' read_more='true' new_window='true' cache='14600']"); ?>
                    </main><!-- #main -->
            </div><!-- #primary -->
        </div>
        </div>
     
   </div>
</section>

        
        

<?php get_footer(); ?>