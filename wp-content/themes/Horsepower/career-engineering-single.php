<?php
/**
 * The template for displaying all single posts.
 *
 * @package parallax-one
 */

	get_header(); 
?>

	</div>
	<!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->
<section class="welcome-main">
		<div class="welcom-cont">
			<div class="greet">
			Hello, <span>Engineer!</span>
		</div>
		<div class="small-txt">
			The team is waiting for you!
			<br />
			<br />
			<br />
			<a href="#content" title="Careers design" data-uk-smooth-scroll="">
			Join our team!
			<!-- <i class="uk-icon uk-icon-arrow-circle-o-down"></i> --></a>
		</div>
		</div>
			<div class="homepage-hero-module">
			    <div class="video-container">
			        <div class="filter"></div>
			        <video autoplay loop class="fillWidth">
			            <source src="https://s3-us-west-2.amazonaws.com/coverr/mp4/Wall-Sketching.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
			            <source src="https://s3-us-west-2.amazonaws.com/coverr/mp4/Wall-Sketching.mp4" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
			        </video>
			        <div class="poster hidden">
			            <img src="PATH_TO_JPEG" alt="">
			        </div>
			    </div>
			</div>
</section>

<div class="content-wrap" id="content">
	<div class="container">
	<div class="uk-width-7-10 uk-container-center">
				<div id="primary" class="content-area <?php if ( is_active_sidebar( 'sidebar' ) ) { echo 'uk-width-1-1 ';} else {echo 'uk-width-1-1';}  ?>">
			<main itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage" id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>
				
				<?php the_post_navigation();  ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					// if ( comments_open() || get_comments_number() ) :
					// 	comments_template();
					// endif;
				?>

			<?php endwhile; // end of the loop. ?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div>


		<?php //get_sidebar(); ?>

	</div>
</div><!-- .content-wrap -->

<?php get_footer(); ?>

