<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package parallax-one
 */

	get_header(); 
?>

	</div>
	<!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->

<div role="main" id="content" class="content-warp">
	<div class="container">

		<div class="uk-grid uk-small-grid uk-margin-large-top">
			<div class="uk-width-medium-7-10 uk-width-small-1-1">
				<div id="primary" class="content-area post-list">
					<main <?php if(have_posts()) echo 'itemscope itemtype="http://schema.org/Blog"'; ?> id="main" class="site-main" role="main">
						<?php if ( have_posts() ) : ?>

							<header class="page-header">
							<?php
								$post = $wp_query->post;
								if (in_category('updates')) {
									 echo "<h3 class='page-title'>News and Updates</h3>";
									}
								elseif(in_category('videos')){
									echo "<h3 class='page-title'>Videos</h3>";
								} 
								else {
									the_archive_title( '<h3 class="">', '</h3>' );
									the_archive_description( '<div class="taxonomy-description">', '</div>' );
								}
							?>
							</header><!-- .page-header -->
							
							<div class="uk-grid uk-grid-small">
							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>

								<?php
									/* Include the Post-Format-specific template for the content.
									 * If you want to override this in a child theme, then include a file
									 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
									 */
										$post = $wp_query->post;
										if (in_category('updates')) {
											 get_template_part( 'content-posts-grid', get_post_format() );
											}
										elseif(in_category('videos')){
											get_template_part( 'content-videos', get_post_format() );
										} 
										else {
											get_template_part( 'content', get_post_format() );
										}
									
								?>

							<?php endwhile; ?>
						</div>
							<?php //the_posts_navigation(); ?>

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

						<?php endif; ?>

					</main><!-- #main -->
				</div><!-- #primary -->
			</div>
			<div class="uk-width-medium-3-10 uk-width-small-1-1">
				<?php get_sidebar(); ?>
			</div>
		</div>

		

	</div>
</div><!-- .content-wrap -->

<?php get_footer(); ?>

