<?php
/**
 * Template name: FAQ
 *
 * @package parallax-one
 */

	get_header(); 
?>

	</div>
	<!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->

<div class="uk-width-1-1">
	<div class="container faq-cont">
		<h1>
			Frequently Asked Questions
		</h1>
		<hr class="uk-divider">

		<h3 class="title">
			<p>
				1.	What is horsepower.ph
			</p>
		</h3>
		<p>
			<b>horsepower.ph </b>is a Philippine-based human resources (HR) products, benefits and services provider company supporting freelancers and entrepreneurs both in the single proprietor space and small and medium scale businesses (SMEs). It is your one stop shop HR and administrative assistance platform. 
		</p>
		<p>
			We promote work-life balance among our members by providing them services which not only enhances the quality of their life, but also empower them to fully enjoy it with the different perks and privileges prepared especially for them!
		</p>
		<p>
			As a financial technology start-up corporation, we specialize in providing benefits ranging from flexible healthcare products, life and accident insurance including travel and consumer protection, online government postings, bill payments, payroll and accounting services, up to our member's need for a working space in jumpstarting their businesses.
		</p>


		<h3 class="title">
			<p>
				2.	What services does horsepower.ph offers?
			</p>
		</h3>
		<p>
			<b>horsepower.ph</b> creates packages that suit different needs of self-employed, entrepreneurs, OFWs and SMEs. Part of our packages include some of the following:
		</p>
		<p>
			<ul>
				<li>
					<p>
						<b>1)</b>  The easy and convenient way of paying government mandated benefits like SSS, Philhealth, and Pagibig and your monthly utility bills through our member's dashboard which is only exclusive to our horsepower member.
					</p>
				</li>
				<li>
					<p>
						<b>2)</b>  Managed Healthcare benefits and Life and Accident Insurance to make sure you're secured 24/7
					</p>
				</li>
				<li>
					<p>
						<b>3)</b>  Access to Legal, Tax and Accounting advice
					</p>
				</li>
				<li>
					<p>
						<b>4)</b>  Access to Online Tools to help with your business and services.
					</p>
				</li>
				<li>
					<p>
						<b>5)</b>  Access to partner co-working and office spaces with different discounts each only for horsepower.ph members.
					</p>
				</li>
			</ul>
		</p>

		<p>
			You can visit <a href="http://www.horsepower.ph">http://horsepower.ph/</a> to check our different packages as well as the benefits and privileges included, under Plans and Pricing.
		</p>
		<h3>
			<p>3.	Who can join and become a member?</p>
		</h3>
		<p>
			horsepower.ph membership is primarily geared towards the needs of self-employed, entrepreneurs and SMEs.
		</p>
		<p>
			If you are an online freelance, work-from-home mom, independent consultant, own an SME, part of a start-up venture or even an OFW, between 18 to 65 years of age, then you need to become a member of horsepower.ph to enjoy the benefits that you deserve.
		</p>

		<h3>
			<p>
				4.	Why should I become a member of horsepower.ph?
			</p>
		</h3>
		<ul class="why-hp">
			<li>
				<p>
					Time is gold and we want you to focus on running your business or completing your projects.
				</p>
			</li>
			<li>
				<p>
					No need to wait in line to pay for your SSS, PhilHealth or Pag-ibig contribution. You can do that using your member's dashboard, even for your utility bills, anytime and anywhere.
				</p>
			</li>
			<li>
				<p>
					Work away from home or conduct meetings at our partner co-working spaces. 
				</p>
			</li>
			<li>
				<p>
					Generate salary computations and adjustments as well as financial reports in one click of a button with readily-available online payroll and accounting systems.
				</p>
			</li>
			<li>
				<p>
					Need legal or tax and accounting-related advice? You have access to our partner lawyers and accountants for consultations at discounted rates.
				</p>
			</li>
			<li>
					<p>
						Being a member of horsepower.ph empowers you with access to all these valuable benefits, products and services that will take care of you and your business needs.
					</p>
			</li>
		</ul>
		<h3>
			<p>
				5.	How much will it cost to become a horsepower.ph member?
			</p>
		</h3>

		<p>
			We know this is one of the major concerns when it comes to joining and becoming a member. We, ourselves were freelancers, entrepreneurs and self-employed before, so we understand your needs and we kept the cost of our membership packages really reasonable and affordable. That's why our most affordable package is <b>only PHP 999.00 a year</b>. Yes, it's once a year!
		</p>
		<p>
			Every package has their different benefits and privileges. For <b>as low as PHP 999.00 to PHP 14, 899.00</b> you can use and enjoy all the benefits and privileges we offer to our members for a year. You may want to check all the packages we offer to our freelancers, entrepreneurs and self-employed, just visit our website horsepower.ph. And from there you can decide which package suited to your needs.
		</p>
		<h3>
			<p>
				6.	What will I get once I become a horsepower.ph member?
			</p>
		</h3>
		<p>
			Once you have completed and processed the payment for your chosen membership package, you will immediately gain access to our member’s dashboard where you can access and use the inclusions of your membership package. Using our member's dashboard, you can now pay your monthly bills as well as your monthly government contributions like SSS, Philhealth, and Pag ibig. 
		</p>
		<p>
			You can now also be part of our Affiliate Program, which is you can refer others to be part of horsepower community. You will be given a system generated a link which you will need to provide or post to social media to let the interested customer register using your link. Successfully registered members will receive an Affiliate Reward. You can visit this link to know more Affiliate Marketing - Referral Program. 
		</p>
		<br />
		 <div class="uk-width-small-1-2 uk-container-center uk-text-center">
            <a href="http://inquiries.horsepower.ph/support/home" class="uk-button uk-button-large uk-width-1-1 uk-button-primary gplan"><i class="uk-icon-info-circle">&nbsp;&nbsp;</i>CLICK HERE FOR MORE INQUIRIES 
            </a>
        </div>
        <br />
	</div>
</div>


<?php get_footer(); ?>