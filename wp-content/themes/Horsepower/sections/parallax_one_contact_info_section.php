<!-- =========================
 SECTION: CONTACT INFO  
============================== -->

<section class="sec-map hp-main-cont uk-width-1-1 uk-margin-large-top uk-margin-large-bottom" style="width:100%;">
	<div class="uk-grid">
		<div class="uk-width-medium-1-1 uk-width-small-1-1">
			<div id="map-canvas"></div>
		</div>
	
<!-- 		<div class="uk-width-medium-3-10 uk-width-small-1-1">
			<div class="uk-width-1-1 hp-contact">
				<h2>Contact HORSEPOWER</h2>	
				<input type="text" class="uk-width-1-1" placeholder="Your Name">
				<input type="text" class="uk-width-1-1" placeholder="Your Email">
				<textarea placeholder="Your Message"></textarea>	

				<a href="#" title="" class="uk-button uk-width-1-1 hp-send">Send</a>
			</div>
		</div> -->
	</div>
</section>

<?php
	$parallax_one_contact_info_item = get_theme_mod('parallax_one_contact_info_content',
		json_encode(
			array( 
					array("icon_value" => "icon-basic-mail" ,"text" => "contact@site.com", "link" => "#" ), 
					array("icon_value" => "icon-basic-geolocalize-01" ,"text" => "Company address", "link" => "#" ), 
					array("icon_value" => "icon-basic-tablet" ,"text" => "0 332 548 954", "link" => "#" ) 
				)
		)
	);

	if( !parallax_one_general_repeater_is_empty($parallax_one_contact_info_item) ){
		$parallax_one_contact_info_item_decoded = json_decode($parallax_one_contact_info_item);
	?>
			<div class="contact-info" id="contactinfo" role="region" aria-label="<?php esc_html_e('Contact Info','parallax-one'); ?>">
				<div class="section-overlay-layer">
					<div class="container" style="width:100%;padding:0;">

						<!-- CONTACT INFO -->
						<div class="row contact-links">

							<?php

								if(!empty($parallax_one_contact_info_item_decoded)){	

										foreach($parallax_one_contact_info_item_decoded as $parallax_one_contact_item){
											if(!empty($parallax_one_contact_item->link)){
												echo '<div class="col-sm-4 contact-link-box col-xs-12">';
												if(!empty($parallax_one_contact_item->icon_value)){	
													echo '<div class="icon-container"><span class="'.esc_attr($parallax_one_contact_item->icon_value).' colored-text"></span></div>';
												}
												if(!empty($parallax_one_contact_item->text)){
													echo '<a href="'.$parallax_one_contact_item->link.'" class="strong">'.html_entity_decode($parallax_one_contact_item->text).'</a>';
												}
												echo '</div>';
											} else {

												echo '<div class="col-sm-4 contact-link-box  col-xs-12">';
												if(!empty($parallax_one_contact_item->icon_value)){
													echo '<div class="icon-container"><span class="'.esc_attr($parallax_one_contact_item->icon_value).' colored-text"></span></div>';
												}
												if(!empty($parallax_one_contact_item->text)){
													if(function_exists('icl_t')){
														echo '<a href="" class="strong">'.icl_t('Contact',$parallax_one_contact_item->id.'_contact',html_entity_decode($parallax_one_contact_item->text)).'</a>';
													} else {
														echo '<a href="" class="strong">'.html_entity_decode($parallax_one_contact_item->text).'</a>';
													}
												}
												echo '</div>';
											}
										}
								}

							?>         
						</div><!-- .contact-links -->
					</div><!-- .container -->
				</div>
			</div><!-- .contact-info -->
<?php
	} 
?>