<style type="text/css" media="screen">
	section.media .uk-grid .uk-width-small-1-1{
		max-height: 500px;
	}
/*buttons*/
	section.media .uk-grid .uk-width-small-1-1 a{
		margin: 15px 0;
	}
	section.media .uk-grid .uk-width-small-1-1:first-child a:first-child{
		background: #3a5795;
    	text-shadow: none;
	}
	section.media .uk-grid .uk-width-small-1-1:nth-child(2) a:first-child{
		background: #39A3E4;
    	text-shadow: none;
	}
	section.media .uk-grid .uk-width-small-1-1:nth-child(3) a:first-child{
		background: #666666;
    	text-shadow: none;
	}
/*icons*/
	section.media .uk-grid .uk-width-small-1-1 i{
    	color: #fff;
	}
	.media-btn{
	    border: 1px solid rgba(158, 8, 8, 0.68);
	    padding: 10px;
	    border-radius: 4px;

	}
	.media-btn:hover{
	    text-decoration: none;
	    
	}
	#twitter-widget-0 {width: 100%;}
</style>
<section class="media">
	<div class="container">
		<div class="uk-width-small-1-1 uk-text-center">
			<h1 class="uk-margin-bottom-remove">Social Media</h1>
				<a href="#" title="" class="uk-margin-top-remove">
					continue to page <i class="uk-icon uk-icon-external-link">&nbsp;&nbsp;</i>
				</a>
		</div>
		<br />
		<div class="uk-grid uk-grid-small">	
			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a class="media uk-icon-button">
                    	<i class="uk-icon uk-icon-facebook"></i>
                	</a>
                    <div class="uk-dropdown uk-dropdown-large uk-dropdown-left drp">
                        <ul class="uk-nav uk-nav-dropdown">
                            <li>
                            	<?php echo do_shortcode("[facebook_likebox]"); ?>
                            </li>
                        </ul>
                    </div>
                </div>
			</div>
			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a class="media uk-icon-button">
                    	<i class="uk-icon uk-icon-twitter"></i>
                	</a>
                    <div class="uk-dropdown uk-dropdown-large uk-dropdown-left drp">
                        <ul class="uk-nav uk-nav-dropdown">
                            <li>
                            	<?php echo do_shortcode("[TWTR]"); ?>
                            </li>
                        </ul>
                    </div>
                </div>
			</div>
			<div class="uk-width-small-1-1 uk-text-center uk-width-medium-1-3">
				<div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center'}">
                    <a class="media uk-icon-button">
                    	<i class="uk-icon uk-icon-linkedin"></i>
                	</a>
                    <div class="uk-dropdown uk-dropdown-large uk-dropdown-left drp">
                        <ul class="uk-nav uk-nav-dropdown">
                            <li>
								<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
								<script type="IN/CompanyProfile" data-id="4847866" data-format="inline"></script>
                            </li>
                        </ul>
                    </div>
                </div>	
		</div>
		<br /><!-- 
		<div class="uk-clearfix">
			<div class="uk-float-right">
				<a class="media-btn">Proceed to our Media Page</a>
			</div>
		</div> -->

	</div>
</section>	
<br />
