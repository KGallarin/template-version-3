<!-- =========================
 SECTION: CLIENTS LOGOs
============================== -->
<div class="uk-width-1-1 uk-text-center uk-margin-large-top">
	<h1 class="hp-headers-txt-white hp-t-hpred" style="color:#333; margin:20px 0;">PARTNERS</h1>
</div>
<?php 
	$parallax_one_logos = get_theme_mod('parallax_one_logos_content',
		json_encode(
			array( 
				array("image_url" => parallax_get_file('/images/companies/1.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/2.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/3.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/4.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/5.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/6.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/7.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/8.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/9.png') ,"link" => "#" ), 
				array("image_url" => parallax_get_file('/images/companies/10.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/11.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/12.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/13.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/14.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/15.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/16.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/17.png') ,"link" => "#" ),
				array("image_url" => parallax_get_file('/images/companies/18.png') ,"link" => "#" ) 
			)
		)
	);
	if(!empty($parallax_one_logos)){
		$parallax_one_logos_decoded = json_decode($parallax_one_logos);
		echo '<div class="clients white-bg" id="clients" role="region" aria-label="'.__('Affiliates Logos','parallax-one').'"><div class="container">';
			echo '<ul class="client-logos">';					
			foreach($parallax_one_logos_decoded as $parallax_one_logo){
				if(!empty($parallax_one_logo->image_url)){
					echo '<li>';
					if(!empty($parallax_one_logo->link)){
						echo '<a href="'.$parallax_one_logo->link.'" title="">';
							echo '<img src="'.$parallax_one_logo->image_url.'" alt="'. esc_html__('Logo','parallax-one') .'">';
						echo '</a>';
					} else {
						echo '<img src="'.esc_url($parallax_one_logo->image_url).'" alt="'.esc_html__('Logo','parallax-one').'">';
					}
					echo '</li>';
				}
			}
			echo '</ul>';
		echo '</div></div>';
	}
?>
	