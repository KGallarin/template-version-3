<?php
/**
 * Template name: Engineering Careers
 *
 * @package parallax-one
 */

	get_header(); 
?>

	</div>
	<!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->
<section class="c-eng"> 
<div class="filter"></div>
<div class="content"></div>
 	<div class="homepage-hero-module">
	    <div class="video-container">
	        <div class="filter"></div>
	        <video autoplay loop class="fillWidth">
	            <source src="<?php echo get_template_directory_uri(); ?>/images/hp-images/mp4/engineering.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
	            <source src="https://s3-us-west-2.amazonaws.com/coverr/mp4/Wall-Sketching.mp4" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
	        </video>
	        <div class="poster hidden">
	            <img src="PATH_TO_JPEG" alt="Horsepower">
	        </div>
	    </div>
	</div>
</section>
<?php get_footer(); ?>


