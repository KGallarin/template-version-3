<?php
/**
 * Template name: About Us
 *
 * @package parallax-one
 */

	get_header(); 
?>

	</div>
	<!-- /END COLOR OVER IMAGE -->
</header>
<!-- /END HOME / HEADER  -->
		<div class="hp-main-container">
			<div class="hp-home-text-bottom">
				<div class="uk-width-1-1">
			        <div id="my-id" class="uk-offcanvas">
					    <div class="uk-offcanvas-bar uk-offcanvas-bar-show">
					        <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
					            <li class="uk-nav-header uk-text-center"><img src="img/navlogo.png" width="90" height="30" title="Horsepower.ph" alt="Horsepower"></li>
					            <li>
					                <a href="index.php"><i class="uk-icon-home uk-icon-medium"></i><span>Home</span></a>
					            </li>
					            <li>
					            <a href="products.php"><i class="uk-icon-cart-plus uk-icon-medium"></i><span>Products</span></a>
					            </li>
					            <li><a href="aboutus.php"><i class="uk-icon-child uk-icon-medium"></i><span>About Us</span></a>
					            </li>
					            <li><a href="careers.php"><i class="uk-icon-graduation-cap uk-icon-medium"></i><span style="margin-left:14px;">Careers</span></a>
					            </li>
					            <li><a href="#"><i class="uk-icon-question uk-icon-medium"></i><span style="margin-left:36px;">FAQ</span></a>
					            </li>
					            <li><a href="#"><i class="uk-icon-newspaper-o uk-icon-medium"></i><span style="margin-left:20px;">Media</span></a>
					            </li>
					        </ul>
					    </div>
					</div>
				</div>

			<div class="uk-grid uk-grid-small hp-headers">
			<section class="uk-width-medium-1-1 uk-width-small-1-1 hp-imgcont">
					<!-- <img src="img/abt.jpg" alt="">	 -->
				    <img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/abt2.jpg" title="Horsepower" alt="Horsepower">
			</section>
			<section class="hp-main-cont">
				<div class="uk-width-medium-1-1 uk-width-small-1-1 hp-texts">
					<div class="uk-grid">
						<div class="uk-width-medium-1-2 uk-width-small-1-1 hp-t-flexer">
							<h2 class="uk-text-left uk-margin-bottom-remove uk-padding-remove title">
								We empower every type of entrepreneur and changing <br />  how benefits works!
							</h2>		
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<h4 class="uk-margin-top">
								Horsepower is a cloud-based SaaS platform of  Human Resource benefits, presented by flexi-bend product tier and managed by a single web and mobile app dashboard.
							</h4>
							<h4>
								We consolidate personal and government bills payment, Healthcare and Insurance, Business tools, Co-working spaces, General tax and legal support, access to SME financing, Virtual assistants, Retail rewards and membership referral rewards and more.
							</h4>	
						</div>
					</div>
				</div>
			</section>
<div id="started"></div> 	
			</div>
			<br />
		</div>
		<div class="hp-main-cont hp-hpteams">
		<br />
		<h2 class="uk-text-left uk-margin-bottom-remove uk-padding-remove hp-ystarted">
			Why we started Horsepower?
		</h2>		
		<br />			
			<div class="uk-grid uk-grid-small">
				<div class="uk-width-medium-1-1 uk-width-small-1-1 started">
					<h4>
						<div class="uk-grid">
							<div class="uk-width-medium-1-2 uk-width-small-1-1">
								<p>
									The Horsepower story started from the pressing need of freelancers to conveniently pay for their Social Security, government postings and provide healthcare for themselves and their family. These are basic benefits that are stripped from corporate employees turned solo entrepreneurs says Jojy. 
								</p>
								<p>
									Diego was involved in Technical Recruiting and Executive Search, mostly for foreign Startups setting up a office in Manila. Working with candidates from large companies to a startup company without details of clear healthcare benefit was a tough experience.
								</p>
								<p>
									One afternoon, we both sat down finding innovative solutions to solve this problem. Diego recalled that although HMO and Healthcare Plans started in early 80's, even until today, the Philippines, and neighboring developing countries are still generally an out-of-pocket means for healthcare. Access to affordable and comprehensive healthcare is limited to companies holding large number of employees. 
								</p>
							</div>
							<div class="uk-width-medium-1-2 uk-width-small-1-1 hp-img-founders">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/founders.jpg" title="Horsepower" alt="Horsepower">
							</div>
						</div>
						<p class="uk-margin-small-top">
							Bootstrapping, we made attempts speaking to large companies for a seamless access to healthcare and payment portals through technology. "We had our share being ignored by these companies". It was not easy for them to understand a potential partnership value of  a technology platform that will remove the tedious processing.
						</p>
					</h4>
					<h4>
						<p>
							Our search for another Co-Founder took us months. One evening, we got introduced to Hannah by a good friend. Her track record of building a network of offline freelancers from 16,000 to 1M in 3 years while providing healthcare benefits as a retention program made us decide that she is the perfect candidate we are looking for.
						</p>
						<p>
							Together, we reconnected with our former business and industry contacts that can possibly take our idea forward. One opportunity lead to another until we completed the details needed to take us to MVP.
						</p>
						<p>
							We did not plan to build a health or bills payment app. We all shared the vision of providing a complete solution for solo and Micro Entrepreneurs that will provide security and protection,  and most, feel the creative freedom to run their business.
						</p>
						<p>
							We want to share our philosophy to all entrepreneurs, whether just starting out, or in the stage of growing their business,  that they deserve a simple, intelligent, affordable and transparent access to all these Human Resource products and services.
						</p>
						<p>Joined by a talented team, together we made the commitment to deliver that promise. </p>

						<p>We are happy to invite you to join us.</p>

						<p><i>Horsepowered,</i></p>
						<p><b>Jojy, Diego and Hannah</b></p>
						
						<div class="uk-width-1-1 hp-socials">
							<span>Share this Story&nbsp;&nbsp;<a href="" class="uk-icon-button uk-icon-facebook" style=""></a></span>
							<span><a href="" class="uk-icon-button uk-icon-linkedin" style=""></a></span>
							<span><a href="" class="uk-icon-button uk-icon-twitter" style=""></a></span>
						</div>
						<div class="uk-width-1-1">
						<br />
							<a href="http://horsepower.ph/category/job-posting/" title="" class="uk-button hp-header-btns hp-rstory uk-margin-remove">Join our team!</a> 
						<br />
						</div>
					</h4>	
				</div>
			</div>
		</div>
		</div>
	<div class="hp-main-cont">
		<div class="uk-grid uk-grid-small">
			<div class="uk-width-small-1-1 uk-width-medium-1-1">
				<div class="hp-text-btm">
					<div class="hr-re">
						<span class="c-text">Membership Plan Benefits</span>
					</div>
				</div>
					<div class="uk-grid hp-ftures">
						<div class="uk-width-medium-1-2 hp-two-cols">
							<div class="uk-grid uk-grid-small">
								<div class="uk-width-medium-3-10 uk-text-center">
									<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/healthcare.png" width="100" title="Horsepower" alt="Horsepower">
								</div>
								<div class="uk-width-medium-7-10">
									Members can avail up to PHP 30,000 MBL per sickness with cashless transactions on accredited hospitals and clinics.
								</div>
							</div>
						</div>
						<div class="uk-width-medium-1-2 hp-two-cols">
							<div class="uk-grid uk-grid-small">
								<div class="uk-width-medium-3-10 uk-text-center">
									<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/insurance.png" width="90">
								</div>
								<div class="uk-width-medium-7-10">
									Get double coverage on life and accident insurance plus access to international medical assistance and emergency services. 
								</div>
							</div>
						</div>

						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/bills-payment.png" title="Horsepower" alt="Horsepower" style="width: 24%;">
								<div class="uk-text-center icon-title">
									utility bills
								</div>
							</p>
							<p>
								Pay your utility bills, from water to cable and internet plans, at your convenience! Here's the <a href="#hp-modal" title="" data-uk-modal="{center:true}">list</a>.					
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/benefits.png" title="Horsepower" alt="Horsepower" style="width: 19%;">
								<div class="uk-text-center icon-title">
									Online Government Posting
								</div>
							</p>
							<p>
								Conveniently pay your monthly government contributions for SSS, Pag-Ibig and PhilHealth online and never miss your deadlines again! Here's the <a href="#hp-modal-gp" title="" data-uk-modal="{center:true}">list</a>.
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/co-working.png" title="Horsepower" alt="Horsepower" style="width: 42%;">
								<div class="uk-text-center icon-title">
									Co-working Space
								</div>
							</p>
							<p>
								Our office space partners are always ready to provide you with comfortable and internet-ready workstations and meeting rooms! Here's our <a href="#hp-modal-cw" title="" data-uk-modal="{center:true}">Office Space Partners</a>
							</p>
						</div>
						            <div id="hp-modal-cw" class="uk-modal">
						                <div class="uk-modal-dialog">
						                <p class="uk-text-right">
						                    <a class="uk-modal-close uk-close uk-text-right"></a>                    
						                </p>
						                    <div class="uk-width-1-1 uk-text-center">
						                        <div class="hp-text-btm uk-margin-top-remove">
						                        <div class="hr-re">
						                            <span class="c-text">Co-working Spaces</span>
						                        </div>
						                    </div>
						                    </div>
						                    <div class="uk-grid bills">
						                        <div class="uk-width-medium-1-3">
						                            <div class="bcont-img">
                        								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/impact1.jpg" style="width: 75%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
						                            </div>
						                            <hr class="uk-divider">
						                        </div>
							                    <div class="uk-width-medium-1-3">
							                            <div class="bcont-img">
                            								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/accelerate1.jpg" style="width: 74%;padding: 12px 0px;" title="Horsepower" alt="Horsepower" style="width: 42%;">
							                            </div>
							                            <hr class="uk-divider">
							                    </div>
							                    <div class="uk-width-medium-1-3">
							                            <div class="bcont-img">
                            								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/kmc.png" style="width: 95%;padding: 8px 0;" title="Horsepower" alt="Horsepower" style="width: 42%;">
							                            </div>
							                            <hr class="uk-divider">
							                    </div>
							                    <div class="uk-width-medium-1-3">
						                            <div class="bcont-img">
                        								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/47east1.jpg" style="width: 85%;padding: 10px 0;" title="Horsepower" alt="Horsepower" style="width: 42%;">
						                            </div>
						                            <hr class="uk-divider">
						                        </div>
							                    <div class="uk-width-medium-1-3">
							                            <div class="bcont-img">
                            								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/dojo1.jpg" style="width: 61%;padding: 13px 0px;" title="Horsepower" alt="Horsepower" style="width: 42%;">
							                            </div>
							                            <hr class="uk-divider">
							                    </div>
							                    <div class="uk-width-medium-1-3">
							                            <div class="bcont-img">
                            								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/rrc1.jpg" style="width: 67%;padding: 8px 0;" title="Horsepower" alt="Horsepower" style="width: 42%;">
							                            </div>
							                            <hr class="uk-divider">
							                    </div>
							                </div>
							            </div>
							        </div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/perks.png" style="width: 17%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
								<div class="uk-text-center icon-title">
									Retail Rewards
								</div>
							</p>
							<p>
								<ul>
									<li>Lifestyle Rewards Program</li>
									<li>Prepaid Master Card</li>
								</ul>
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/taxlegal.jpg" style="width: 38%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
								<div class="uk-text-center icon-title">
									Tax & Legal Support
								</div>
							</p>
							<p>
								Access general tax and legal inquiries for free from our knowledge base. Get discounted rates from our partner lawyers for more specific and task related requests. Details will be shown on on your membership dashboard.
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/payroll.png" style="padding: 20px 0;" title="Horsepower" alt="Horsepower" style="width: 42%;">
								<div class="uk-text-center icon-title">
									Online Payroll & Accounting System
								</div>
							</p>
							<p>
								Manage your team's compensation packages through our online payroll system. Generate salary computations and adjustments in one click of a button! 
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/referral.png" style="width: 29%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
								<div class="uk-text-center icon-title">
									Referral Program
								</div>
							</p>
							<p>
								Your membership entitles you to earn from successful referrals. Tell your friends how amazing Horsepower benefits and share your experience. <a href="http://horsepower.ph/referal/form" title="">REFER NOW</a>
							</p>
						</div>
						<div class="uk-width-small-1-1 uk-width-medium-1-4">
							<p class="uk-text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/busicard.png" style="width: 29%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
								<div class="uk-text-center icon-title">
									Business Cards
								</div>
							</p>
							<p>
								Print your initial set of business cards on us!
							</p>
						</div>
					</div>
			</div>
		</div>
	            <!-- This is the modal -->
            <div id="hp-modal" class="uk-modal">
                <div class="uk-modal-dialog">
                <p class="uk-text-right">
                    <a class="uk-modal-close uk-close uk-text-right"></a>                    
                </p>
                    <div class="uk-width-1-1 uk-text-center">
                        <div class="hp-text-btm uk-margin-top-remove">
                        <div class="hr-re">
                            <span class="c-text">Pay your bills on</span>
                        </div>
                    </div>
                    </div>
                    <div class="uk-grid bills">
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/bayantel.png" style="width: 75%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="bills-cont">
                                 - Bayan DSL
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/Digitel_Logo.png" style="width: 75%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="bills-cont">
                                 - Digitel
                                 <br />
                                 - Digitel DSL Plus
                                 <br />
                                 - Digitel Business DSL
                            </div>
                        </div>
                         <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/globe1.png" style="padding: 6px 0px;width: 78%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="bills-cont">
                                 - Globe Lines
                                 <br />
                                 - Globe Broadband
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/maynilad.png" style="padding: 8px 0px;width: 81%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="bills-cont">
                                 - Maynilad
                            </div>
                        </div>
                        <div class="uk-width-1-1 also uk-text-center">
                            <br />
                        </div>
                         <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pldt.png" style="padding: 8px 0px;width: 81%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="bills-cont">
                                 - PLDT
                                 <br />
                                 - PLDT MyDSL
                            </div>
                        </div>
                         <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/sky.png" style="padding: 13px 0px;width: 80%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="bills-cont">
                                 - Sky Cable
                                 <br />
                                 - Sky Internet
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/smart.jpg" style="padding: 4px 0px;width: 79%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="bills-cont">
                                 - Smart
                                 <br />
                                 - Smart Broadband
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/sun.png" style="padding: 0px 0px;width: 71%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                            <div class="bills-cont">
                                 - Sun Broadband
                                 <br />
                                 - Sun Cellular
                                 <br />
                                 - Sun Cable System
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- GP Modal -->

            <div id="hp-modal-gp" class="uk-modal">
                <div class="uk-modal-dialog">
                <p class="uk-text-right">
                    <a class="uk-modal-close uk-close uk-text-right"></a>                    
                </p>
                    <div class="uk-width-1-1 uk-text-center">
                        <div class="hp-text-btm uk-margin-top-remove">
                        <div class="hr-re">
                            <span class="c-text">Government Postings</span>
                        </div>
                    </div>
                    </div>
                    <div class="uk-grid bills">
                    <div class="uk-width-medium-1-3">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/pagibig1.jpg" style="width: 74%;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                    </div>
                    <div class="uk-width-medium-1-3">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/sss.png" style="width: 99%;padding: 18px 0px;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                        </div>
                    <div class="uk-width-medium-1-3">
                            <div class="bcont-img">
								<img src="<?php echo get_template_directory_uri(); ?>/images/hp-images/philhealth1.jpg" style="width: 95%;padding: 30px 0;" title="Horsepower" alt="Horsepower" style="width: 42%;">
                            </div>
                            <hr class="uk-divider">
                    </div>
                </div>
            </div>
        </div>
    </div>    
 <br />
 <br />
 		<section class="uk-width-1-1 careers-cont uk-margin-large-top">
			<section class="hp-main-cont">
					<div class="uk-grid uk-grid-small hp-headers">
						<div class="uk-width-medium-1-1 uk-width-small-1-1 hp-texts">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-width-small-1 hp-c-flexer">
									<h2 class="uk-text-left uk-margin-bottom-remove uk-padding-remove title-c">
										We are <span data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:400, repeat: true}">HORSEPOWER!</span>
									</h2>		
								</div>
								<div class="uk-width-medium-1-2 uk-width-small-1">
									<h4 class="uk-margin-top">
										Inspiring. Empowering. Rewarding. Fun. These are some of the words people commonly use to describe their careers at Horsepower!
									</h4>	
									<div class="uk-width-1-1">
										<br />
											<a href="http://horsepower.ph/category/job-posting/" title="" class="uk-button hp-header-btns hp-rstory uk-margin-remove">Join our team!</a>
										<br />
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="uk-grid career-title">
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/engineering/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Engineering
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/design/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Design
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/sales-and-marketing/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Sales and Marketing (Community, Field Sales, Affiliate, Digital Marketing)
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/product-and-business-development/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Product and Business Development
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="#" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Operations and Finance
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
						<div class="uk-width-medium-1-2 uk-width-small-1-1">
							<a href="http://horsepower.ph/category/job-posting/social-services/" title="">
								<div class="uk-clearfix">
									<span class="uk-float-left pos">
										Social (Client services, PR and Media relations)
									</span>
									<span class="uk-float-right go">
										<i class="uk-icon-arrow-right"></i>
									</span>
								</div>	
							</a>
						</div>
					</div>
			</section>
		</section>
<?php get_footer(); ?>
